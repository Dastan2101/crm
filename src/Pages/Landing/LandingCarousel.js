import React from 'react';
import ItemsCarousel from "react-items-carousel";
import BloggerCard from "./BloggerCard/BloggerCard";
import CircleArrowIcon from "../../components/kit/Icons/CircleArrowIcon";

const autoPlayDelay = 5000;
const chevronWidth = 40;

export default class AutoPlayCarousel extends React.Component {
    state = {
        activeItemIndex: 0,
    };

    componentDidMount() {
        this.interval = setInterval(this.tick, autoPlayDelay);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    tick = () => this.setState(prevState => ({
        activeItemIndex: (prevState.activeItemIndex + 1) % (this.props.bloggers.length - this.props.cardItem + 1),
    }));

    onChange = value => this.setState({activeItemIndex: value});

    render() {
        return (
            <ItemsCarousel
                gutter={this.props.bloggers.length}
                numberOfCards={this.props.cardItem}
                activeItemIndex={this.state.activeItemIndex}
                requestToChangeActive={this.onChange}
                leftChevron={<CircleArrowIcon className="card-left-arrow"/>}
                rightChevron={<CircleArrowIcon className="card-right-arrow"/>}
                chevronWidth={chevronWidth}
                outsideChevron
            >
                {this.props.bloggers.map(item => {
                    if (item.blogger.image) {
                        return <BloggerCard
                            key={item.id}
                            data={item}/>
                    }
                })}
            </ItemsCarousel>
        );
    }
}

