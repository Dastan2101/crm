import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

const LandingExpansionPanel = ({title, children}) => {
    return (
        <ExpansionPanel className="landing__expansion">
            <ExpansionPanelSummary
                aria-controls="panel1a-content"
                id="panel1a-header"
            >
                <p className="landing__expansion-title">{title}</p>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <p className="landing__expansion-text">{children}</p>
            </ExpansionPanelDetails>
        </ExpansionPanel>
    );
};

export default LandingExpansionPanel;