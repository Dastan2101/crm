import React, {useEffect, useState} from 'react';
import {
    ROUTE_ADMIN,
    ROUTE_ADVERTISE_PAGE,
    ROUTE_ADVERTISEMENT, ROUTE_ADVERTISEMENT_BLOGGERS, ROUTE_ADVERTISEMENTS,
    ROUTE_APPLICATIONS_ARCHIVE,
    ROUTE_APPLICATIONS_BLOGGERS,
    ROUTE_APPLICATIONS_COMPANY,
    ROUTE_BLOGGERS, ROUTE_REPORT, ROUTE_SETTINGS
} from "../../global/Routes";
import {history} from "../../store/configureStore";
import ApplicationsCompany from "../ApplicationsCompany/ApplicationsCompany";
import './index.scss';
import Dashboard from "../Dashboard/Dashboard";
import ApplicationsBlogger from "../ApplicationBlogger/ApplicationsBlogger";
import ApplicationsArchive from "../ApplicationsArchive/ApplicationsArchive";
import Bloggers from "../Bloggers/Bloggers";
import {ThemeProvider} from "@material-ui/core/styles";
import {createMuiTheme} from "@material-ui/core/styles";
import Advertisement from "../Advertisement/Advertisement";
import ChooseBlogger from "../ChooseBlogger/ChooseBlogger";
import Advertisements from "../Advertisement/Advertisements";
import AdvertiseCompany from "../Advertisement/AdvertiseCompany";
import Settings from "../Settings/Settings";
import Report from "../Report/Report";
import Main from "../Main/Main";
import {setNotifications} from "../../store/applications/applicationsActions";
import {connect} from "react-redux";
import {NotificationManager} from 'react-notifications';

const SOCKET_URI = 'ws://62.109.29.111:8000/ws/notifications/'

const Admin = (props) => {
    const theme = createMuiTheme({
        typography: {body1: {fontSize: "16px"}}
    });

    useEffect(function () {
        let socket = new WebSocket(SOCKET_URI)

        socket.onmessage = function (event) {
            const decodedMessage = JSON.parse(event.data);
            props.setNotifications(decodedMessage)
            if (decodedMessage && decodedMessage.hasNewApplications) {
                NotificationManager.warning("У вас новое уведомление");
            }
        }

        socket.onclose = () => {
            // automatically try to reconnect on connection loss
            socket = new WebSocket(SOCKET_URI)
        }

        return function () {
            socket.close()
        }
    }, [])

    return (
        <ThemeProvider theme={theme}>
            <div className="admin">
                <Dashboard {...props}>
                    <div className="admin__content">
                        {history.location.pathname === ROUTE_ADMIN && <Main/>}
                        {history.location.pathname === ROUTE_APPLICATIONS_COMPANY && <ApplicationsCompany/>}
                        {history.location.pathname === ROUTE_APPLICATIONS_BLOGGERS && <ApplicationsBlogger/>}
                        {history.location.pathname === ROUTE_APPLICATIONS_ARCHIVE && <ApplicationsArchive/>}
                        {history.location.pathname === ROUTE_BLOGGERS && <Bloggers/>}
                        {history.location.pathname === ROUTE_SETTINGS && <Settings/>}
                        {history.location.pathname === ROUTE_ADVERTISEMENTS && <Advertisements/>}
                        {history.location.pathname === ROUTE_REPORT && <Report/>}
                        {history.location.pathname.includes(ROUTE_ADVERTISEMENT) &&
                        props.match.params.advertisementId &&
                        <Advertisement {...props}/>}
                        {history.location.pathname.includes(ROUTE_ADVERTISEMENT_BLOGGERS) &&
                        props.match.params.advertisementId &&
                        <ChooseBlogger {...props}/>}
                        {history.location.pathname.includes(ROUTE_ADVERTISE_PAGE) &&
                        props.match.params.advertisementId &&
                        <AdvertiseCompany {...props}/>}
                    </div>
                </Dashboard>
                }
            </div>
        </ThemeProvider>
    );
};

const mapDispatchToProps = dispatch => ({
    setNotifications: (data) => dispatch(setNotifications(data))
})

export default connect(null, mapDispatchToProps)(Admin);