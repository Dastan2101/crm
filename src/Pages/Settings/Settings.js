import React, {useEffect, useState} from 'react';
import {
    fetchAdvertisements, fetchAudienceGet, fetchContentTypesGet, fetchCreateAudienceAge,
    fetchRemoveAdvertType, fetchRemoveAudienceAge, fetchRemoveContentType,
    fetchRemoveSocial, fetchRemoveStatus,
    fetchSocialsGet, fetchStatusesGet
} from "../../store/settings/settingsActions";
import {connect} from "react-redux";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Table from "@material-ui/core/Table";
import './index.scss';
import AdvertisementTypeItem from "./AdvertisementTypeItem";
import SettingsModal, {
    AdvertisementType, AUDIENCE_TYPE, CONTENT_TYPE,
    SOCIAL_NETWORK, STATUSES_TYPE
} from "../../components/AdminModal/Modals/Settings/SettingsModal";
import SocialNetworkItem from "./SocialNetworkItem";
import ContentTypeItem from "./ContentTypeItem";
import StatusItem from "./StatusItem";
import AudienceAgeItem from "./AudienceAgesItem";

const TABLE_TYPE_NAME = {
    1: "Тип рекламы",
    2: "Название соц. сети",
    3: "Тематика",
    4: "Название статуса",
    5: "Аудитория"
}

const Settings = (
    {
        advertisement_types,
        fetchAdvertisements,
        settingTypeId,
        fetchRemoveAdvertType,

        fetchSocialsGet,
        socials,
        fetchRemoveSocial,

        contentTypes,
        fetchContentTypesGet,
        fetchRemoveContentType,

        fetchStatusesGet,
        statuses,
        fetchRemoveStatus,

        audienceAges,
        fetchAudienceGet,
        fetchRemoveAudienceAge
    }
) => {

    const [selectedItem, setItem] = useState(null)


    useEffect(function () {
        if (settingTypeId === AdvertisementType) {
            fetchAdvertisements()
        }
        if (settingTypeId === SOCIAL_NETWORK) {
            fetchSocialsGet()
        }
        if (settingTypeId === CONTENT_TYPE) {
            fetchContentTypesGet()
        }
        if (settingTypeId === STATUSES_TYPE) {
            fetchStatusesGet()
        }
        if (settingTypeId === AUDIENCE_TYPE) {
            fetchAudienceGet()
        }
    }, [settingTypeId])

    function onEdit(item) {
        setItem(item)
    }

    return (
        <>
            <div className="settings-root">
                <Table className="application-blogger__table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="left">Дата создания</TableCell>
                            <TableCell align="left">ID</TableCell>
                            {(settingTypeId === SOCIAL_NETWORK || settingTypeId === STATUSES_TYPE) &&
                            <TableCell align="left">Иконка</TableCell>
                            }
                            <TableCell align="left" className={
                                settingTypeId === SOCIAL_NETWORK ? "settings-root__social" : "settings-root__name"}>
                                {TABLE_TYPE_NAME[settingTypeId]}
                            </TableCell>
                            <TableCell align="right">Действия</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {settingTypeId === AdvertisementType &&
                        advertisement_types &&
                        Array.isArray(advertisement_types) &&
                        advertisement_types.sort((a, b) => a.id - b.id).map(item => (
                            <AdvertisementTypeItem
                                key={item.id}
                                item={item}
                                onEdit={onEdit}
                                onRemove={fetchRemoveAdvertType}
                            />
                        ))}
                        {settingTypeId === AUDIENCE_TYPE &&
                        audienceAges &&
                        Array.isArray(audienceAges) &&
                        audienceAges.sort((a, b) => a.id - b.id).map(item => (
                            <AudienceAgeItem
                                key={item.id}
                                item={item}
                                onEdit={onEdit}
                                onRemove={fetchRemoveAudienceAge}
                            />
                        ))}
                        {settingTypeId === SOCIAL_NETWORK &&
                        socials && Array.isArray(socials) &&
                        socials.sort((a, b) => a.id - b.id).map(item => (
                            <SocialNetworkItem
                                onEdit={onEdit}
                                key={item.id}
                                onRemove={fetchRemoveSocial}
                                item={item}/>
                        ))
                        }

                        {settingTypeId === CONTENT_TYPE &&
                        contentTypes && Array.isArray(contentTypes) &&
                        contentTypes.sort((a, b) => a.id - b.id).map(item => (
                            <ContentTypeItem
                                onEdit={onEdit}
                                key={item.id}
                                onRemove={fetchRemoveContentType}
                                item={item}/>
                        ))
                        }

                        {settingTypeId === STATUSES_TYPE &&
                        statuses && Array.isArray(statuses) &&
                        statuses.sort((a, b) => a.id - b.id).map(item => (
                            <StatusItem
                                onEdit={onEdit}
                                key={item.id}
                                onRemove={fetchRemoveStatus}
                                item={item}/>
                        ))}
                    </TableBody>
                </Table>
            </div>
            {selectedItem &&
            <SettingsModal
                onClose={() => setItem(null)}
                typeId={settingTypeId}
                type="edit"
                initValues={selectedItem} />
            }
        </>
    );
};

const mapStateToProps = state => ({
    advertisement_types: state.settings.advertisement_types,
    settingTypeId: state.settings.settingTypeId,

    socials: state.settings.socials,
    contentTypes: state.settings.contentTypes,
    statuses: state.settings.statuses,

    audienceAges: state.settings.audienceAges
})

const mapDispatchToProps = dispatch => ({
    fetchAdvertisements: data => dispatch(fetchAdvertisements(data)),
    fetchRemoveAdvertType: id => dispatch(fetchRemoveAdvertType(id)),

    fetchSocialsGet: () => dispatch(fetchSocialsGet()),
    fetchRemoveSocial: id => dispatch(fetchRemoveSocial(id)),

    fetchContentTypesGet: () => dispatch(fetchContentTypesGet()),
    fetchRemoveContentType: id => dispatch(fetchRemoveContentType(id)),

    fetchStatusesGet: () => dispatch(fetchStatusesGet()),
    fetchRemoveStatus: id => dispatch(fetchRemoveStatus(id)),

    fetchAudienceGet: () => dispatch(fetchAudienceGet()),
    fetchRemoveAudienceAge: id => dispatch(fetchRemoveAudienceAge(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(Settings);