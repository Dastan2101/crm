import React, {useState} from 'react';
import TableCell from "@material-ui/core/TableCell";
import IconButton from "@material-ui/core/IconButton";
import MoreIcon from "../../components/kit/Icons/MoreIcon";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import EditIcon from "../../components/kit/Icons/EditIcon";
import TrashIcon from "../../components/kit/Icons/TrashIcon";
import TableRow from "@material-ui/core/TableRow";

const AdvertisementTypeItem = ({item, onEdit, onRemove}) => {


    const [anchorDotEl, setAnchorDotEl] = React.useState(null);

    const isOpen = Boolean(anchorDotEl)

    const handleDotClick = (event) => {
        setAnchorDotEl(event.currentTarget);
    };

    const handleDotClose = () => {
        setAnchorDotEl(null);
    };

    const edit = (item) => {
        onEdit(item)
        setAnchorDotEl(null);
    }

    const remove = (id) => {
        onRemove(id)
        setAnchorDotEl(null);
    }

    return (
        <TableRow>
            <TableCell align="left">{item.created_date}</TableCell>
            <TableCell align="left">{item.id}</TableCell>
            <TableCell align="left">{item.type_name}</TableCell>
            <TableCell align="right">
                <IconButton
                    aria-label="more"
                    aria-controls="long-menu"
                    aria-haspopup="true"
                    className="dot-rotate-icon"
                    onClick={handleDotClick}
                >
                    <MoreIcon color="#6A7083" className="icon-rotate"/>
                </IconButton>
                <Menu
                    anchorEl={anchorDotEl}
                    keepMounted
                    open={isOpen}
                    onClose={handleDotClose}
                >
                    <MenuItem className="header-options-list" onClick={() => edit(item)}>
                        <EditIcon className="icon"/> <span className="text">Редактировать</span>
                    </MenuItem>
                    <MenuItem className="header-options-list" onClick={() => remove(item.id)}>
                        <TrashIcon className="icon" color="#FA445C"/>
                        <span className="text">Удалить</span>
                    </MenuItem>
                </Menu>
            </TableCell>
        </TableRow>
    );
};

export default AdvertisementTypeItem;