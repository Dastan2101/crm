import React, {useState} from 'react';
import ava from "../../assets/logo_icon.png";
import {EXCLUSIVE_STATUS_NAME, GENDER_TYPES} from "../../global/constants";
import MoreIcon from "../../components/kit/Icons/MoreIcon";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import EditIcon from "../../components/kit/Icons/EditIcon";
import TrashIcon from "../../components/kit/Icons/TrashIcon";
import CheckIcon from "../../components/kit/Icons/CheckIcon";

const BloggerItemList = ({data, onEdit, onRemove, onSelect, collection, idsList}) => {
    const year = data.birthday_date && new Date(data.birthday_date);
    const age = year && new Date().getFullYear() - year.getFullYear();

    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    }

    function edit() {
        onEdit(data.id)
        setAnchorEl(null);
    }

    const remove = () => {
        setAnchorEl(null);
        onRemove(data.id)
    }

    return (
        <div className="list-item">
            <div className="list-item__header">
                <div className="list-item__header-img">
                    <img src={data.image || ava} alt="#"/>
                    <span className="rating">{data.er}</span>
                </div>
                <div className="list-item__header-names">
                    <p className={`status-name 
                    ${data.status_name === EXCLUSIVE_STATUS_NAME
                        ? "status-name--exclusive"
                        : ""}`}>
                        <img src={data.status_image} alt="" className="status-image"/>
                        {data.status_name}
                        {!data.telegram_status && (
                            <span className="list-item__header-names--status-lg">
                                не активен
                            </span>
                        )}
                    </p>
                    <p className="name">
                        <span>{data.middle_name && ` ${data.middle_name.charAt(0)}.`}</span>
                        <span>{data.name && ` ${data.name.charAt(0)}.`}</span>
                        <span>{data.surname}</span>
                        {age ? `, ${age}` : ""}
                    </p>
                    <p className="gender-country">
                        {data.gender && GENDER_TYPES[data.gender]},
                        <span> {data.country_name}</span>
                    </p>
                </div>
            </div>
            <div className="list-item__body">
                <div className="list-item__socials">
                    {data.social_network && data.social_network.map((social, ndx) => {
                        if (ndx < 3) {
                            return (
                                <div key={social.id} className="list-item__social">
                                    <a rel="noopener noreferrer" target="_blank" href={social.soc_network_url}
                                       className="link">
                                        <img className="icon" src={social.social_network_image} alt=""/>
                                    </a>
                                    <span className="subscribers-count">
                                {social.subscribers}
                            </span>
                                </div>
                            )
                        } else return null
                    })}
                </div>
                <div className="list-item__services">
                    {data.service && data.service.map((service, ndx) => {
                        if (ndx < 3) {
                            return (
                                <div key={service.id} className="list-item__service">
                                    <span className="link">{service.advertisement_type_name}</span>
                                    <span className="price">
                                От {service.price}c
                            </span>
                                </div>
                            )
                        } else return null
                    })}

                </div>
                <div className="list-item__subjects">
                    {data.subject_list && data.subject_list.map((item, ndx) => {
                        if (ndx < 4) {
                            return (
                                <div key={item.id} className="subject">
                            <span className="subject-name">
                                {item.subject_name}
                            </span>
                                </div>
                            )
                        } else return null
                    })}

                </div>
            </div>
            <button
                className={`list-item__btn
                 ${idsList && idsList.includes(data.id) && "list-item__btn--black"}
                 ${!data.telegram_status && 'list-item__btn--disabled'}`}
                onClick={onSelect}
                disabled={!data.telegram_status && !!collection}
            >
                {idsList && idsList.includes(data.id) &&
                <>
                    <CheckIcon color="#fff" width={16} height={16}/>
                    Отменить
                </>
                }

                {idsList && !idsList.includes(data.id) && collection && "Добавить"}

                {!collection && "Подробнее"}

            </button>
            {!collection &&
            <div className="list-item__options">
                <IconButton
                    aria-label="more"
                    aria-controls="long-menu"
                    aria-haspopup="true"
                    onClick={handleClick}
                >
                    <MoreIcon color="#6A7083"/>
                </IconButton>
                <Menu
                    id="long-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={open}
                    onClose={handleClose}
                    PaperProps={{
                        style: {
                            width: '20ch',
                        },
                    }}
                >
                    <MenuItem className="header-options-list" onClick={edit}>
                        <EditIcon className="icon"/> <span className="text">Редактировать</span>
                    </MenuItem>
                    <MenuItem className="header-options-list" onClick={remove}>
                        <TrashIcon className="icon" color="#FA445C"/>
                        <span className="text">Удалить</span>
                    </MenuItem>
                </Menu>
            </div>}
        </div>
    );
};

export default React.memo(BloggerItemList);