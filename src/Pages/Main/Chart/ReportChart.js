import React from 'react';
import useChartConfig from './userChartConfig'
import {Chart} from 'react-charts'

const ReportChart = ({initCompanies, initBloggers}) => {
    const {data} = useChartConfig({
        dataType: 'ordinal',
        initCompanies,
        initBloggers,
        fillColor: ["rgba(0,10,220,0.5)","rgba(220,0,10,0.5)","rgba(220,0,0,0.5)","rgba(120,250,120,0.5)" ],
        strokeColor: "rgba(220,220,220,0.8)",
        highlightFill: "rgba(220,220,220,0.75)",
        highlightStroke: "rgba(220,220,220,1)",
    })
    const series = React.useMemo(
        () => ({
            type: 'bar'
        }),
        []
    )
    const axes = React.useMemo(
        () => [
            {primary: true, type: 'ordinal', position: 'bottom'},
            {position: 'left', type: 'linear', stacked: false}
        ],
        []
    )
    return (
        <div style={{width: "100%", height: "300px"}}>
            <Chart data={data} series={series} axes={axes} tooltip />
        </div>
    );
};

export default ReportChart;