import React, {useEffect} from 'react';
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import {makeStyles} from "@material-ui/core/styles";
import './index.scss';
import LargeBellIcon from "../../components/kit/Icons/LargeBellIcon";
import BullHorn from "../../components/kit/Icons/BullHorn";
import StarLargeIcon from "../../components/kit/Icons/StarLargeIcon";
import {fetchStatisticsInfo} from "../../store/report/reportActions";
import {connect} from "react-redux";
import {fetchBloggers} from "../../store/bloggers/bloggersActions";
import CommonSpinner from "../../components/kit/CommonSpinner/CommonSpinner";
import ava from "../../assets/logo_icon.png";
import {NavLink} from "react-router-dom";
import {ROUTE_BLOGGERS} from "../../global/Routes";
import NoResultIcon from "../../components/kit/Icons/NoResultIcon";
import ReportChart from "./Chart/ReportChart";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paperSmall: {
        padding: "40px",
        backgroundColor: "#fff",
        borderRadius: "10px",
        boxShadow: "0px 2px 0px #EDEDF6",
        height: "151px",
        boxSizing: "border-box"
    },
    paper: {
        padding: "40px",
        backgroundColor: "#fff",
        borderRadius: "10px",
        boxShadow: "0px 2px 0px #EDEDF6",
    },
}));

const Main = (
    {fetchStatisticsInfo, statistics, fetchBloggers, requested, bloggersList}
) => {
    const classes = useStyles();

    useEffect(function () {
        fetchStatisticsInfo()
        fetchBloggers({date: new Date()})
    }, [])

    return (
        <div className="main-root">
            <Grid container spacing={3}>
                <Grid item xs={12} lg={8} className="main-root__cards-root">
                    <Paper className={[classes.paperSmall, "main-root__paper"].join(" ")}>
                        <div className="main-root__paper-icon --violet-bg">
                            <LargeBellIcon/>
                        </div>
                        <div className="main-root__paper-info">
                            <h4 className="main-root__paper-info-count --violet">
                                {statistics && statistics.blogger_request}
                            </h4>
                            <p className="main-root__paper-info-title">
                                Заявки на блогерство
                            </p>
                        </div>
                    </Paper>

                    <Paper className={[classes.paperSmall, "main-root__paper"].join(" ")}>
                        <div className="main-root__paper-icon --dark-blue-bg">
                            <LargeBellIcon color="#2C59E5"/>
                        </div>
                        <div className="main-root__paper-info">
                            <h4 className="main-root__paper-info-count --dark-blue">
                                {statistics && statistics.company_request}
                            </h4>
                            <p className="main-root__paper-info-title">
                                Заявки от компаний
                            </p>
                        </div>
                    </Paper>

                    <Paper className={[classes.paperSmall, "main-root__paper"].join(" ")}>
                        <div className="main-root__paper-icon --yellow-bg">
                            <BullHorn/>
                        </div>
                        <div className="main-root__paper-info">
                            <h4 className="main-root__paper-info-count --yellow">
                                {statistics && statistics.ad_campaign}
                            </h4>
                            <p className="main-root__paper-info-title">
                                Рекламных компаний
                            </p>
                        </div>
                    </Paper>

                    <Paper className={[classes.paperSmall, "main-root__paper"].join(" ")}>
                        <div className="main-root__paper-icon --green-bg">
                            <StarLargeIcon/>
                        </div>
                        <div className="main-root__paper-info">
                            <h4 className="main-root__paper-info-count --green">
                                {statistics && statistics.all_bloggers}
                            </h4>
                            <p className="main-root__paper-info-title">
                                Всего блогеров
                            </p>
                        </div>
                    </Paper>
                </Grid>
                <Grid item xs={12} lg={4}>
                    <Paper className={[classes.paper, "main-root__bloggers"].join(" ")}>
                        <div className="main-root__bloggers-head">
                            <h2>Новые блогеры</h2>
                            <span>За сегодня</span>
                        </div>
                        {requested && <CommonSpinner isLoading={requested} centered/>}
                        {!requested && bloggersList && (
                            <ul>
                                {bloggersList.count > 0 &&
                                bloggersList.results.map(item => (
                                    <li key={item.id}>
                                        <img src={item.image || ava} alt="#"/>
                                        <p>
                                            <span>{item.surname}</span>
                                            <span>&nbsp;{item.name}</span>
                                        </p>
                                    </li>
                                ))}
                            </ul>)}

                        {!requested && bloggersList && bloggersList.count === 0 && (
                            <div className="no-result">
                                <NoResultIcon width={100} height={140}/>
                                <p className="no-result__text">
                                    На сегодня новых блогеров нет
                                </p>
                            </div>
                        )}

                        {bloggersList && bloggersList.count > 0 && (
                            <NavLink className="main-root__bloggers-nav" to={ROUTE_BLOGGERS}>
                                Показать всех
                            </NavLink>
                        )}
                    </Paper>
                </Grid>
            </Grid>

            <div className="main-root__chart-root">
                {statistics && (
                <Paper className={[classes.paper, "main-root__chart-paper"].join(" ")}>
                    <div className="main-root__chart-header">
                        <h2>статистика заявок</h2>
                        <div className="main-root__chart-labels">
                            <p className="compaing">Заявки рекл. кампаний</p>
                            <p className="bloggers">Заявки на блогерство</p>
                        </div>
                    </div>
                    <ReportChart initCompanies={statistics.company_month} initBloggers={statistics.blogger_month}/>
                </Paper>
                )}
            </div>

        </div>
    );
};

const mapStateToProps = state => ({
    statistics: state.reports.statistics,
    requested: state.bloggers.requested,
    bloggersList: state.bloggers.bloggersList
})

const mapDispatchToProps = dispatch => ({
    fetchStatisticsInfo: () => dispatch(fetchStatisticsInfo()),
    fetchBloggers: (filter) => dispatch(fetchBloggers(filter))
})

export default connect(mapStateToProps, mapDispatchToProps)(Main);