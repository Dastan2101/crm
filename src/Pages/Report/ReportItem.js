import React from 'react';
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import {numberWithSpaces} from "../../global/helpers";

const ReportItem = ({data}) => {
    return (
            <TableRow className="report-root__item">
                <TableCell align="left">{data.id}</TableCell>
                <TableCell align="left">{data.campaign_name}</TableCell>
                <TableCell align="left">{data.customer}</TableCell>
                <TableCell align="left">{data.date}</TableCell>
                <TableCell align="left">{data.deadline}</TableCell>
                <TableCell align="left">{numberWithSpaces(data.budget)}c</TableCell>
                <TableCell align="left">{numberWithSpaces(data.top_commission)}c</TableCell>
                <TableCell align="left">{numberWithSpaces(data.lower_commission)}c</TableCell>
                <TableCell align="left">{numberWithSpaces(data.blogger_spending)}c</TableCell>
                {/*<TableCell align="center">...</TableCell>*/}
            </TableRow>
    );
};

export default ReportItem;