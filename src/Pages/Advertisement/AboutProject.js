import React, {useState} from 'react';
import {convertDateWithMonthName, dateFormatter, numberWithSpaces} from "../../global/helpers";
import DownloadIcon from "../../components/kit/Icons/DownloadIcon";
import FileTextIcon from "../../components/kit/Icons/FileTextIcon";
import GeneralButton from "../../components/kit/GeneralButton/GeneralButton";
import {CURRENT_DATE} from "../../global/constants";
import DatePickerForm from "../../components/kit/DatePickerForm/DatePickerForm";
import moment from "moment";
import {isEmptyArray} from "formik";

export const IN_PROGRESS_STATUS = "P";
export const COMPLETE_STATUS = "D";

const AboutProject = ({data, sendTz, downloadFile, onComplete}) => {

    const [deadline, setDeadline] = useState(moment(data.deadline, "DD-MM-YYYY").toDate())

    function dateHandleChange(date) {
        setDeadline(date)
        onComplete({deadline: dateFormatter(date)}, data.id, true)
    }

    let advertiseCount = 0;

    data.blogger && data.blogger.forEach(blogger => {
        blogger.bill && blogger.bill.forEach(() => {
            advertiseCount++
        })
    })

    function sendForComplete() {
        onComplete({status: COMPLETE_STATUS}, data.id)
    }

    function exportFile() {
        downloadFile(data)
    }


    return (
        <div className="advertisement__about-project">
            <div className="advertisement__about-project-holder">
                <div className="papers-info">
                    <div className="info-paper">
                        <h6 className="paper-title">Дата создания</h6>
                        <p className="paper-sub-title">{convertDateWithMonthName(data.date)}</p>
                    </div>

                    <div className="info-papers">
                        <div className="info-paper info-paper--short">
                            <h6 className="paper-title">Общий бюджет</h6>
                            <p className="paper-sub-title">
                                {numberWithSpaces(data.budget)}
                            </p>
                        </div>
                        <div className="info-paper info-paper--short">
                            <h6 className="paper-title">Комиссия</h6>
                            <p className="paper-sub-title">
                                {numberWithSpaces(data.agent_comission_sum)}
                            </p>
                        </div>
                        <div className="info-paper info-paper--short">
                            <h6 className="paper-title">Компания</h6>
                            <p className="paper-sub-title">
                                {data.campaign_name}
                            </p>
                        </div>
                        <div className="info-paper info-paper--short">
                            <h6 className="paper-title">ФИО Заказчика</h6>
                            <p className="paper-sub-title">
                                {data.customer}
                            </p>
                        </div>
                        <div className="info-paper info-paper--short">
                            <h6 className="paper-title">Дедлайн</h6>
                            <DatePickerForm
                                className="info-paper__datepicker"
                                selected={deadline}
                                onChange={dateHandleChange}
                                minDate={CURRENT_DATE}
                            />
                            {/*<p className="paper-sub-title">*/}

                            {/*    /!*{convertDateWithMonthName(data.deadline)}*!/*/}
                            {/*</p>*/}
                        </div>
                        <div className="info-paper info-paper--short">
                            <h6 className="paper-title">Реклам</h6>
                            <p className="paper-sub-title">
                                {advertiseCount}
                            </p>
                        </div>

                        {!isEmptyArray(data.blogger) && (
                            <div className="info-paper-export">
                                <h6 className="commercial-text">
                                    <FileTextIcon/> Коммерческое предложение
                                </h6>
                                <button onClick={exportFile}><DownloadIcon/>
                                    Скачать
                                </button>
                            </div>
                        )}

                        {data.is_created && !data.is_sent &&
                        data.status !== COMPLETE_STATUS &&
                        data.blogger &&
                        !isEmptyArray(data.blogger) &&
                        <GeneralButton onSubmit={sendTz}>
                            Отправить тз блогерам
                        </GeneralButton>
                        }

                        {data.is_sent && data.status === IN_PROGRESS_STATUS &&
                        <GeneralButton onSubmit={sendForComplete}>
                            Завершить
                        </GeneralButton>
                        }
                    </div>

                </div>
                <div className="tz_text">
                    <div className="tz-paper">
                        <h3>Техническое задание компании</h3>
                        <p>{data.specification}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AboutProject;