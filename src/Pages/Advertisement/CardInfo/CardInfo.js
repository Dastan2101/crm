import React, {useState} from 'react';
import {cutText, getPercentage, numberWithSpaces} from "../../../global/helpers";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import GeneralButton from "../../../components/kit/GeneralButton/GeneralButton";
import {isEmptyArray} from "formik";
import {COMPLETE_STATUS} from "../AboutProject";

const TABS = ['Итоговая', 'О проекте'];

const MAX_TEXT_LENGTH = 200;

const CardInfo = ({percent, data, onCreate}) => {
    const [activeTab, setTab] = useState(0);
    const [more, showMore] = useState(false);

    const [isActive, setActive] = useState(false);

    const [percentValue, setPercent] = useState(percent || "")

    function handleChange(e) {
        e.persist()
        setActive(true)
        setPercent(e.target.value)
    }

    const names = data.customer && data.customer.split(" ");

    let totalBloggerSum = 0;
    let totalCommission = 0;
    data.blogger && data.blogger.forEach(item => {
        item.bill && item.bill.forEach(bill => {
            totalBloggerSum += bill.price;
        })
        totalCommission += item.commission
    })

    let totalBlogSumWithCommis = totalBloggerSum + totalCommission;
    const totalPercentSum = getPercentage(totalBlogSumWithCommis, percentValue);

    const totalSum = totalBlogSumWithCommis + totalPercentSum;

    function onSubmit() {
        const editData = {
            is_created: true,
            agent_comission_percent: Number(percentValue),
            budget: totalSum
        }
        onCreate(editData, data.id, !data.is_created);
        setActive(false)
    }

    return (
        <div className="advertisement__info">
            <div className="advertisement__info-tabs">
                {TABS.map((tab, ndx) => (
                    <p className={`advertisement__info-tab 
                    ${activeTab === ndx && "advertisement__info-tab--active"}`}
                       onClick={() => setTab(ndx)}
                       key={ndx}>
                        {tab}
                    </p>
                ))}
            </div>
            <div className="advertisement__info-items">
                {activeTab === 1 &&
                <div className="advertisement__info-project">
                    <ul>
                        <li>
                            <span>ID проекта</span>
                            <span>#{data.id}</span>
                        </li>
                        <li>
                            <span>Компания</span>
                            <span>{data.campaign_name}</span>
                        </li>
                        <li>
                            <span>Заказчик ФИО</span>
                            {names &&
                            <span>
                                {names[0] && names[0]}.
                                {names[1] && `${names[1].charAt(0)}.`}
                                {names[2] && `${names[2].charAt(0)}.`}
                            </span>}
                        </li>
                        <li>
                            <span>Телефон</span>
                            <span>{data.phone_number}</span>
                        </li>
                        <li>
                            <span>Дедлайн</span>
                            <span>{data.deadline}</span>
                        </li>
                    </ul>
                    <p className="sub-title">Тех. задание</p>
                    <p className="text">
                        {!more ?
                            cutText(data.specification, MAX_TEXT_LENGTH)
                            : data.specification}
                    </p>
                    {data.specification && data.specification.length > MAX_TEXT_LENGTH &&
                    <button className="show-more" onClick={() => showMore(!more)}>
                        {!more ? "Показать еще" : "Свернуть"}
                    </button>}
                </div>
                }
                {activeTab === 0 &&
                <div className="advertisement__info-totals">
                    <ul>
                        <li>
                            <span>Выбрано блогеров</span>
                            <span>{data.blogger && data.blogger.length}</span>
                        </li>
                        <li>
                            <span>Сумма блогеров</span>
                            <span>{totalBloggerSum ? numberWithSpaces(totalBloggerSum) : "00.00"}</span>
                        </li>
                        <li>
                            <span>Ниж.комиссия</span>
                            <span> {totalCommission ? numberWithSpaces(totalCommission) : "00.00"}</span>
                        </li>
                        <li>
                            <span>Сумма ниж.к</span>
                            <span>{totalBlogSumWithCommis ? numberWithSpaces(totalBlogSumWithCommis) : "00.00"}</span>
                        </li>
                        <li>
                            <span>Агентская комиссия</span>
                            <Input
                                value={percentValue}
                                onChange={handleChange}
                                type="number"
                                step={1}
                                disabled={data.status === COMPLETE_STATUS}
                                endAdornment={<InputAdornment position="end">%</InputAdornment>}
                            />
                        </li>
                        <li>
                            <span>Сумма аг.к</span>
                            <span>
                                {totalCommission
                                    ? numberWithSpaces(totalPercentSum)
                                    : "00.00"}
                            </span>
                        </li>
                        <li>
                            <span>Итого бюджет</span>
                            <span className="green">
                                {
                                    totalSum
                                        ? numberWithSpaces(totalSum)
                                        : "00.00"
                                }
                            </span>
                        </li>
                    </ul>

                    {data.status !== COMPLETE_STATUS &&
                    <GeneralButton onSubmit={onSubmit}
                                   disabled={(data.blogger && isEmptyArray(data.blogger)) || !isActive}>
                        {data.is_created ? "Редактировать" : "Создать компанию"}
                    </GeneralButton>}
                </div>
                }
            </div>

        </div>
    );
};

export default CardInfo;