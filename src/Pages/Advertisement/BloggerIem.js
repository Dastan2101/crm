import React from 'react';
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import {isEmptyArray} from "formik";
import CheckIcon from "../../components/kit/Icons/CheckIcon";
import IconButton from "@material-ui/core/IconButton";
import MoreIcon from "../../components/kit/Icons/MoreIcon";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import EditIcon from "../../components/kit/Icons/EditIcon";
import TrashIcon from "../../components/kit/Icons/TrashIcon";
import {numberWithSpaces} from "../../global/helpers";
import avatar from '../../assets/avatar.png';
import Tooltip from "@material-ui/core/Tooltip";
import {AGREEMENT_STATUS, ANSWER_TZ_STATUSES, REJECTED_STATUS, WAITING_STATUS} from "../../global/constants";
import FileTextIcon from "../../components/kit/Icons/FileTextIcon";
import MailIcon from "../../components/kit/Icons/MailIcon";
import CheckCircleIcon from "../../components/kit/Icons/CheckCircleIcon";
import PowerIcon from "../../components/kit/Icons/PowerIcon";

const BloggerItem = (
    {
        data,
        onSelect,
        onRemove,
        percent,
        statusTz,
        disabled,
        sendPersonalMailing,
        editStatus,
    }) => {

    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    }

    function edit() {
        onSelect({...data, type: "prices"})
        setAnchorEl(null);
    }

    function editTz() {
        onSelect({...data, type: "tz"})
        setAnchorEl(null);
    }

    const remove = () => {
        setAnchorEl(null);
        onRemove(data.id, data.campaign)
    }

    function sendMailing() {
        sendPersonalMailing && sendPersonalMailing(data.id);
        setAnchorEl(null);
    }

    function onEditStatus(data, bloggerid) {
        editStatus(data, bloggerid)
        setAnchorEl(null);
    }

    return (
        <TableRow className="advertisement__item">
            <TableCell align="left">
                <span className="avatar">
                    <img src={data.image || avatar} alt="#"/>
                    <span className={`rating 
                    ${data.er <= 3 ?
                        "rating--red" :
                        data.err <= 6
                            ? "rating--yellow" :
                            ""}`}>
                        <span>{data.er}</span>
                    </span>
                </span>
                <span className="name">
                    {data.surname}
                    {data.name && ` ${data.name.charAt(0)}.`}
                    {data.middle_name && ` ${data.middle_name.charAt(0)}`}
                </span>
            </TableCell>
            <TableCell align="left">
                {isEmptyArray(data.bill)
                    ? <button
                        onClick={() => onSelect({...data, type: "prices"})}
                        className="button-select">
                        Выбрать
                    </button>
                    :
                    data.bill.map(it => (
                        <React.Fragment key={it.id}>
                            {it.advertisement_name.length > 7 ?
                                <Tooltip title={it.advertisement_name}>
                                    <span className="bill_name">{it.advertisement_name}</span>
                                </Tooltip>
                                : <span className="bill_name">{it.advertisement_name}</span>}
                        </React.Fragment>
                    ))
                }
            </TableCell>
            <TableCell align="left">
                {isEmptyArray(data.bill)
                    ? <span className="empty">00.00</span>
                    :
                    data.bill.map(it => (
                        <p key={it.id} className="bill_price">{numberWithSpaces(it.price)}</p>
                    ))
                }
            </TableCell>
            <TableCell align="left">
                {isEmptyArray(data.bill) ? <span className="empty">00.00</span> :
                    numberWithSpaces(data.bill.reduce((acc, curr) =>
                        acc + (Math.floor(Number(curr.price) * Number(curr.commission) / 100)), 0))
                }

            </TableCell>
            {/*{!statusTz &&*/}
            {/*<TableCell align="left">*/}
            {/*    <span className="empty">{percent}%</span>*/}
            {/*</TableCell>*/}
            {/*}*/}
            <TableCell align="left">
                <span className={`${data.total_sum === 0 ? "empty" : ""}`}>
                    {numberWithSpaces(data.total_sum)}
                    {/*{data.bill.length ? data.bill.map(it => numberWithSpaces(it.total_sum)) : '00.00'}*/}
                </span>
            </TableCell>
            <TableCell align="left">
                {
                    !data.text_tz
                        ? <button
                            className="button-select"
                            onClick={() => onSelect({...data, type: "tz"})}
                        >
                            Написать
                        </button>
                        : <span className="complete">
                            <span className="check-icon">
                                <CheckIcon width={20} height={20} color="#fff"/>
                            </span>
                            Есть
                        </span>
                }
            </TableCell>
            {statusTz &&
            <TableCell align="center">
                {data.answer &&
                <span className={`empty 
                ${data.answer === AGREEMENT_STATUS
                    ? "green-text" : data.answer === REJECTED_STATUS
                        ? "red-text" : "grey-bold"}`}>
                    {ANSWER_TZ_STATUSES[data.answer || WAITING_STATUS]}
                </span>}
                {data.answer && data.answer === REJECTED_STATUS &&
                <button className="show-reject"
                        onClick={() => onSelect({...data, type: "rejected"})}>Смотреть</button>
                }
            </TableCell>}
            <TableCell align="center">
                <IconButton
                    aria-label="more"
                    aria-controls="long-menu"
                    aria-haspopup="true"
                    className="edit-rotate-icon"
                    onClick={handleClick}
                    disabled={disabled}
                >
                    <MoreIcon color="#6A7083" className="icon-rotate"/>
                </IconButton>
                <Menu
                    id="long-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={open}
                    onClose={handleClose}
                >
                    <MenuItem className="header-options-list" onClick={edit}>
                        <EditIcon className="icon"/> <span className="text">Редактировать цены</span>
                    </MenuItem>
                    <MenuItem className="header-options-list" onClick={editTz}>
                        <FileTextIcon className="icon"/> <span className="text">Редактировать ТЗ</span>
                    </MenuItem>

                    {!!sendPersonalMailing && (
                        <MenuItem
                            className="header-options-list"
                            onClick={sendMailing}
                            disabled={!data.text_tz || isEmptyArray(data.bill)}
                        >
                            <MailIcon className="icon"/> <span className="text">Отправить ТЗ</span>
                        </MenuItem>
                    )}

                    {!!editStatus && (
                        <MenuItem className="header-options-list"
                                  disabled={data.answer === AGREEMENT_STATUS}
                                  onClick={() => onEditStatus({answer: AGREEMENT_STATUS}, data.id)}>
                            <CheckCircleIcon color="#24AF47" className="icon"/> <span className="text">Согласен</span>
                        </MenuItem>
                    )}

                    {!!editStatus && (
                        <MenuItem className="header-options-list"
                                  disabled={data.answer === REJECTED_STATUS}
                                  onClick={() => onEditStatus({answer: REJECTED_STATUS}, data.id)}>
                            <PowerIcon width={20} height={20} className="icon"/> <span className="text">Отказано</span>
                        </MenuItem>
                    )}

                    <MenuItem className="header-options-list" onClick={remove}>
                        <TrashIcon className="icon" color="#FA445C"/>
                        <span className="text">Удалить</span>
                    </MenuItem>
                </Menu>
            </TableCell>
        </TableRow>
    );
};

export default BloggerItem;