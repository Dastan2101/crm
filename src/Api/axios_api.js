import axios from 'axios';
import {ROUTE_AUTH} from "../global/Routes";
export const apiURL = 'http://178.238.228.92:8087/';

const instance = axios.create({
    baseURL: apiURL
});

/**
 * Global API Error Handling
 */
instance.interceptors.response.use((r) => r, (error) => {

    if (
        error.response
        && error.response.status === 401
        && !window.location.pathname.startsWith(ROUTE_AUTH)
    ) {
        localStorage.setItem("bl_key", null)
        window.location.pathname = ROUTE_AUTH;
        return;
    }

    return Promise.reject(error);
});

export default instance;
