// User
export const ROUTE_USER_AUTHORIZATION = '/rest-auth/login/';
export const ROUTE_USER_LOGOUT = '/rest-auth/logout/';

// Landing
export const ROUTE_LANDING = '/landing/landing';
export const ROUTE_LANDING_BLOGGERS = '/landing/bloggers/';
export const ROUTE_LANDING_PARTNERS = '/landing/partners/';
export const ROUTE_LANDING_FAQ = '/landing/faq/';
export const ROUTE_LANDING_PROMO_BLOGGERS = '/landing/bloggers_planet/';
export const ROUTE_PROJECT_SOCIALS_GET = 'landing/project_social_network/'

// Applications
export const ROUTE_COMPANY_APPLICATIONS = '/request/companies/';
export const ROUTE_BLOGGER_APPLICATIONS = '/request/bloggers/';
export const ROUTE_ADVERTISE_COMPANY_CREATE = 'request/companies/?create_campaign=true'

// Advertisement
export const ROUTE_ADVERTISEMENT = '/campaign/campaign/';
export const ROUTE_ADVERTISEMENT_BILL = '/campaign/campaign_blogger/';
export const ROUTE_ADVERTISEMENT_SEND_TO_BLOGGERS = '/tg/telegram_mailing/';
export  const ROUTE_ADVERTISEMENT_DOWNLOAD ='/campaign/commercial_offer/'

// Social networks
export const ROUTE_SOCIAL_NETWORKS = '/core/social_network/'

// Bloggers
export const ROUTE_BLOGGERS = '/blogger/blogger/'
export const ROUTE_BLOGGER_STATUSES = '/blogger/status/'
export const ROUTE_BLOGGER_FILTERS = '/blogger/get_filters/'
export const ROUTE_BLOGGER_OFFER = 'campaign/individual_specification/';
export const ROUTE_CHECK_TELEGRAM_LOGIN = '/blogger/check_tg/'


// Core
export const ROUTE_COUNTRIES = '/core/country/'
export const ROUTE_CONTENT_TYPES = '/core/subject/'
export const ROUTE_AUDIENCE_AGE = '/core/audience_age/'
export const ROUTE_ADVERTISEMENT_TYPES = '/core/rektype/'
export const ROUTE_BLOGGER_STATUS = '/blogger/status/'
export const ROUTE_SEO = '/core/seo/'

// Reports
export const ROUTE_REPORT = '/campaign/get_report/'
export const ROUTE_REPORT_JSON = '/campaign/report/'

// Dashboard
export const ROUTE_STATISTICS = '/core/statistics/'

// TG mailing
export const ROUTE_PERSONAL_MAILING = 'tg/blogger_mailing/'