import {
    CLEAR_BLOGGER_INFO,
    FETCH_ADVERTISEMENT_TYPES,
    FETCH_AUDIENCE_AGE,
    FETCH_BLOGGER_BY_ID_FAILURE,
    FETCH_BLOGGER_BY_ID_REQUEST,
    FETCH_BLOGGER_BY_ID_SUCCESS,
    FETCH_BLOGGER_FILTER_OPTIONS, FETCH_BLOGGER_OFFER,
    FETCH_BLOGGER_STATUSES,
    FETCH_BLOGGERS_FAILURE,
    FETCH_BLOGGERS_REQUEST,
    FETCH_BLOGGERS_SUCCESS, FETCH_CHECK_TELEGRAM_LOGIN, FETCH_CLEAR_BLOGGER_STORE,
    FETCH_CONTENT_TYPES,
    FETCH_COUNTRIES_STATUSES,
    FETCH_CREATE_BLOGGER_FAILURE
} from "./bloggersActions";

const initialState = {
    requested: false,
    bloggersList: null,
    bloggersError: null,

    bloggerStatuses: null,

    countries: null,

    contentTypes: null,

    audienceAges: null,

    advertTypes: null,

    bloggerCreateError: null,

    filterOptions: null,

    bloggerRequest: false,
    blogger: null,
    bloggerFailure: null,

    offer: null,

    existsLogin: false
}


export default function bloggersReducer
    (
        state = initialState,
        action
    ) {
    switch (action.type) {
        case FETCH_BLOGGERS_REQUEST:
            return {...state, requested: true};
        case FETCH_BLOGGERS_SUCCESS:
            return {...state, bloggersList: action.bloggers, requested: false, bloggersError: null}
        case FETCH_BLOGGERS_FAILURE:
            return {...state, bloggersError: action.error, requested: false};
        case FETCH_BLOGGER_STATUSES:
            return {...state, bloggerStatuses: action.statuses}

        case FETCH_COUNTRIES_STATUSES:
            return {...state, countries: action.countries}

        case FETCH_CONTENT_TYPES:
            return {...state, contentTypes: action.content}

        case FETCH_AUDIENCE_AGE:
            return {...state, audienceAges: action.data}

        case FETCH_ADVERTISEMENT_TYPES:
            return {...state, advertTypes: action.types}

        case FETCH_CREATE_BLOGGER_FAILURE:
            return {...state, bloggerCreateError: action.error}

        case FETCH_BLOGGER_FILTER_OPTIONS:
            return {...state, filterOptions: action.filters}

        case FETCH_BLOGGER_BY_ID_REQUEST:
            return {...state, bloggerRequest: true}

        case FETCH_BLOGGER_BY_ID_SUCCESS:
            return {...state, blogger: action.blogger, bloggerRequest: false};
        case CLEAR_BLOGGER_INFO:
            return {...state, blogger: null}
        case FETCH_CLEAR_BLOGGER_STORE:
            return {...state, blogger: null}
        case FETCH_BLOGGER_BY_ID_FAILURE:
            return {...state, bloggerRequest: false, bloggerFailure: action.error}

        case FETCH_BLOGGER_OFFER:
            return {...state, offer: action.offer}

        case FETCH_CHECK_TELEGRAM_LOGIN:
            return {...state, existsLogin: action.existsLogin}
        default: {
            return state;
        }
    }
}