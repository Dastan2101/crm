import axios from '../../Api/axios_api';
import {
    ROUTE_LANDING,
    ROUTE_LANDING_BLOGGERS,
    ROUTE_LANDING_FAQ,
    ROUTE_LANDING_PARTNERS,
    ROUTE_LANDING_PROMO_BLOGGERS, ROUTE_SEO
} from "../../Api/Routes";

export const FETCH_LANDING_SUCCESS = 'FETCH_LANDING_SUCCESS';
const fetchLandingSuccess = data => ({type: FETCH_LANDING_SUCCESS, data});

export const fetchLandingInfo = () => {
    return dispatch => {
        return axios.get(ROUTE_LANDING).then(
            response => {
                dispatch(fetchLandingSuccess(response.data))
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_LANDING_BLOGGERS_SUCCESS = 'FETCH_LANDING_BLOGGERS_SUCCESS';
const fetchBloggersSuccess = bloggers => ({type: FETCH_LANDING_BLOGGERS_SUCCESS, bloggers});
export const fetchBloggers = () => {
    return dispatch => {
        return axios.get(ROUTE_LANDING_BLOGGERS).then(
            response => {
                dispatch(fetchBloggersSuccess(response.data))
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_LANDING_PARTNERS_SUCCESS = 'FETCH_LANDING_PARTNERS_SUCCESS';
const fetchLandingPartnersSuccess = partners => ({type: FETCH_LANDING_PARTNERS_SUCCESS, partners});
export const fetchPartners = () => {
    return dispatch => {
        return axios.get(ROUTE_LANDING_PARTNERS).then(
            response => {
                dispatch(fetchLandingPartnersSuccess(response.data))
            }
        ).catch(err => console.log(err))
    }
}


export const FETCH_LANDING_FAQ_SUCCESS = 'FETCH_LANDING_FAQ_SUCCESS';
const fetchLandingFaqSuccess = data => ({type: FETCH_LANDING_FAQ_SUCCESS, data});
export const fetchQuestions = () => {
    return dispatch => {
        return axios.get(ROUTE_LANDING_FAQ).then(
            response => {
                dispatch(fetchLandingFaqSuccess(response.data))
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_LANDING_PROMO_BLOGGERS_SUCCESS = 'FETCH_LANDING_PROMO_BLOGGERS_SUCCESS';
const fetchLandingPromoBloggersSuccess = data => ({type: FETCH_LANDING_PROMO_BLOGGERS_SUCCESS, data});
export const fetchPromoBloggers = () => {
    return dispatch => {
        return axios.get(ROUTE_LANDING_PROMO_BLOGGERS).then(
            response => {
                dispatch(fetchLandingPromoBloggersSuccess(response.data))
            }
        ).catch(err => console.log(err))
    }
}


export const FETCH_SEO = 'FETCH_SEO';
const fetchSeoSuccess = metaData => ({type: FETCH_SEO, metaData});
export const fetchSeo = () => {
    return dispatch => {
        return axios.get(ROUTE_SEO).then(
            response => {
                dispatch(fetchSeoSuccess(response.data))
            }
        )
    }
}