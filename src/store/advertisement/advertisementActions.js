import axios from '../../Api/axios_api';
import {
    ROUTE_ADVERTISEMENT,
    ROUTE_ADVERTISEMENT_BILL,
    ROUTE_ADVERTISEMENT_DOWNLOAD,
    ROUTE_ADVERTISEMENT_SEND_TO_BLOGGERS, ROUTE_PERSONAL_MAILING
} from "../../Api/Routes";
import {NotificationManager} from "react-notifications";
import {history} from "../configureStore";
import {AGREEMENT_STATUS, ERROR_TEXT} from "../../global/constants";
import {ROUTE_ADVERTISE_PAGE, ROUTE_APPLICATIONS_COMPANY} from "../../global/Routes";
import {dateFormatter} from "../../global/helpers";
import {saveAs} from 'file-saver';
import {COMPLETE_STATUS} from "../../Pages/Advertisement/AboutProject";


export const FETCH_ADVERTISEMENT_REQUEST = 'FETCH_ADVERTISEMENT_REQUEST';
const fetchAdvertisementRequest = () => ({type: FETCH_ADVERTISEMENT_REQUEST});

export const FETCH_GET_ADVERTISE_BY_ID = 'FETCH_GET_ADVERTISE_BY_ID';
const fetchGetAdvertiseSuccess = data => ({type: FETCH_GET_ADVERTISE_BY_ID, data});
export const fetchAdvertise = (id, status = "") => {
    let url = `?answer=${status}`
    return dispatch => {
        dispatch(fetchAdvertisementRequest())
        return axios.get(ROUTE_ADVERTISEMENT + id + "/" + url).then(
            response => {
                dispatch(fetchGetAdvertiseSuccess(response.data))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
                history.push(ROUTE_APPLICATIONS_COMPANY)
            }
        ).catch(err => {
            console.log(err)
        })
    }
}

export const fetchEditAdvertisement = (data, id, creatable) => {
    return dispatch => {
        dispatch(fetchAdvertisementRequest())
        return axios.put(ROUTE_ADVERTISEMENT + id + "/", data).then(
            (response) => {
                if (creatable) {
                    history.push(ROUTE_ADVERTISE_PAGE + `/${id}`)
                } else if (data.status === COMPLETE_STATUS) {
                    history.push(`/rekcampaign`)
                } else {
                    history.push(`/advertisement/${id}`)
                }
                dispatch(fetchAdvertise(id))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchEditBloggerBill = (data, bloggerId, advertiseId) => {
    return dispatch => {
        dispatch(fetchAdvertisementRequest())
        return axios.put(ROUTE_ADVERTISEMENT_BILL + bloggerId + "/", data).then(
            (response) => {
                if (advertiseId) {
                    dispatch(fetchAdvertise(advertiseId))
                } else {
                    if (data.answer === AGREEMENT_STATUS) {
                        NotificationManager.success("Заявка успешно принята");
                        history.push("/");
                    } else {
                        setTimeout(function () {
                            history.push("/");
                        }, 3000)
                    }
                }
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchEditBloggerOfferStatus = (offerId, data) => {
    return dispatch => {
        return axios.put(ROUTE_ADVERTISEMENT_BILL + offerId + "/edit/", data).then(
            () => {
                if (data.answer === AGREEMENT_STATUS) {
                    NotificationManager.success("Заявка успешно принята");
                    history.push("/");
                } else {
                    setTimeout(function () {
                        history.push("/");
                    }, 2000)
                }
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchEditBloggerStatus = (data, bloggerId, advertiseId) => {
    return dispatch => {
        dispatch(fetchAdvertisementRequest())
        return axios.put(ROUTE_ADVERTISEMENT_BILL + bloggerId + "/", data).then(
            (response) => {
                dispatch(fetchAdvertise(advertiseId))
                NotificationManager.success("Статус успешно изменен");
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchRemoveAdvertisementBlogger = (bloggerId, advertiseId) => {
    return dispatch => {
        return axios.delete(ROUTE_ADVERTISEMENT_BILL + bloggerId + "/").then(
            () => {
                dispatch(fetchAdvertise(advertiseId))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_ADVERTISEMENTS_SUCCESS = 'FETCH_ADVERTISEMENTS_SUCCESS';
export const FETCH_ADVERTISEMENTS_REQUEST = 'FETCH_ADVERTISEMENTS_REQUEST';

const fetchAdvertisementsRequest = () => ({type: FETCH_ADVERTISEMENTS_REQUEST});
const fetchAdvertisementsSuccess = data => ({type: FETCH_ADVERTISEMENTS_SUCCESS, data});
export const fetchAdvertisements = (page = 1, status = "P", selectedDate, order, search) => {
    let url = ""
    url += `?page=${page}`
    url += `&status=${status}`

    if (selectedDate) {
        url += `&date=${dateFormatter(selectedDate)}`
    }
    if (search) {
        url += `&search=${search}`
    }
    if (order) {
        url += `&order=${order.value}`
    }
    return dispatch => {
        dispatch(fetchAdvertisementsRequest())
        return axios.get(ROUTE_ADVERTISEMENT + url).then(
            response => {
                dispatch(fetchAdvertisementsSuccess(response.data))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchRemoveAdvertisement = id => {
    return dispatch => {
        return axios.delete(ROUTE_ADVERTISEMENT + id + "/").then(
            () => {
                NotificationManager.success("Рекламная компания успешно удалена")
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchSendAdvertisementToBloggers = (data) => {
    return dispatch => {
        return axios.post(ROUTE_ADVERTISEMENT_SEND_TO_BLOGGERS, data).then(
            (response) => {
                if (response.data) {
                    if (response.data.error_msgs) {
                        response.data.error_msgs.forEach((name, ndx) => {
                            NotificationManager.error(`Не удалось отправить письмо для ${name}`, "");
                        })
                    }
                    if (response.data.success_msgs) {
                        response.data.success_msgs.forEach((name, ndx) => {
                            NotificationManager.info(`Письмо для ${name} отправлено успешно`, "", 5000);
                        })
                    }

                }
                dispatch(fetchAdvertise(data.campaign))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const downloadFile = (campaign) => {
    return dispatch => {
        return axios.get(ROUTE_ADVERTISEMENT_DOWNLOAD + campaign.id + "/", {
            responseType: 'arraybuffer'
        }).then(
            response => {
                const blob = new Blob([response.data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                saveAs(blob, `коммерческое_предложение.xls`)
            }
        ).catch(err => console.log(err))
    }
}


export const fetchPersonalTz = (campaign, blogger) => {
    return dispatch => {
        return axios.get(ROUTE_PERSONAL_MAILING + `?campaign=${campaign}&blogger=${blogger}`).then(
            (response) => {
                if (response.data) {
                    if (response.data.error_msgs.length) {
                        NotificationManager.error(`Не удалось отправить письмо для ${response.data.error_msgs[0]}`, "");
                    }
                    if (response.data.success_msgs.length) {
                        NotificationManager.info(`Письмо для ${response.data.success_msgs[0]} отправлено успешно`, "", 5000);
                    }

                }
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}