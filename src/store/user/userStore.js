import {FETCH_USER_AUTHORIZATION_FAILURE, FETCH_USER_AUTHORIZATION_SUCCESS, FETCH_USER_LOGOUT} from "./userActions";

const initialState = {
    user: null,

    userError: null,
}

export default function userReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_USER_AUTHORIZATION_SUCCESS:
            return {...state, user: action.auth, userError: null};
        case FETCH_USER_AUTHORIZATION_FAILURE:
            return {...state, userError: action.error}
        case FETCH_USER_LOGOUT:
            return {...state, user: null, userError: null}
        default:
            return state;
    }
}