import axios from '../../Api/axios_api';
import {ROUTE_REPORT, ROUTE_REPORT_JSON, ROUTE_STATISTICS} from "../../Api/Routes";
import {NotificationManager} from "react-notifications";
import {ERROR_TEXT} from "../../global/constants";
import {dateFormatter} from "../../global/helpers";
import {saveAs} from 'file-saver';

export const FETCH_REPORTS_REQUEST = 'FETCH_REPORTS_REQUEST';
export const fetchReportRequest = () => ({type: FETCH_REPORTS_REQUEST});

export const FETCH_REPORTS = 'FETCH_REPORTS';
const fetchReportsSuccess = reports => ({type: FETCH_REPORTS, reports});
export const fetchReports = (startPeriod, endPeriod) => {
    let url = "";
    if (startPeriod && endPeriod) {
        url = `?start_date=${dateFormatter(startPeriod)}&end_date=${dateFormatter(endPeriod)}`
    }
    return dispatch => {
        dispatch(fetchReportRequest())
        return axios.get(ROUTE_REPORT_JSON + url).then(
            response => {
                dispatch(fetchReportsSuccess(response.data))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchExportReports = (startPeriod, endPeriod) => {
    let url = "";
    if (startPeriod && endPeriod) {
        url = `?start_date=${dateFormatter(startPeriod)}&end_date=${dateFormatter(endPeriod)}`
    }
    return dispatch => {
        return axios.get(ROUTE_REPORT + url, {
            responseType: 'arraybuffer'
        }).then(
            response => {
                const blob = new Blob([response.data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                saveAs(blob, `отчет_за_период_${dateFormatter(startPeriod)}_${dateFormatter(endPeriod)}.xls`)
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}



export const FETCH_STATISTICS_INFO = 'FETCH_STATISTICS_INFO';
const fetchStatisticsInfoSuccess = data => ({type: FETCH_STATISTICS_INFO, data});

export const fetchStatisticsInfo = () => {
    return dispatch => {
        return axios.get(ROUTE_STATISTICS).then(
            response => {
                dispatch(fetchStatisticsInfoSuccess(response.data))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}