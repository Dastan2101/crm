import {FETCH_REPORTS, FETCH_REPORTS_REQUEST, FETCH_STATISTICS_INFO} from "./reportActions";


const initialState = {
    reports: null,

    requested: false,

    statistics: null
}

export default function reportReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_REPORTS_REQUEST:
            return {...state, requested: true}
        case FETCH_REPORTS:
            return {...state, reports: action.reports, requested: false}
        case FETCH_STATISTICS_INFO:
            return {...state, statistics: action.data}
        default:
            return state;
    }
}