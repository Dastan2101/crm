import React from 'react';
import './App.scss';
import {connect} from "react-redux";
import Routes from "./global/Routes";
import {NotificationContainer} from "react-notifications";

const App = ({user}) => {
    return (
        <>
            <Routes user={user}/>
            <NotificationContainer/>
        </>
    );
}

const mapStateToProps = state => ({
    user: state.user.user
})

export default connect(mapStateToProps)(App);
