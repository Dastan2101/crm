import React, {useRef, useState} from 'react';
import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";
import logo from '../../assets/avatar.png';
import Popper from "@material-ui/core/Popper";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import MenuList from "@material-ui/core/MenuList";
import MenuItem from "@material-ui/core/MenuItem";
import ArrowSimpleIcon from "../kit/Icons/ArrowSimpleIcon";
import PowerIcon from "../kit/Icons/PowerIcon";
import ApplicationCompanyModal from "../AdminModal/Modals/ApplicationCompanyModal";
import ApplicationBlogerModal from "../AdminModal/Modals/ApplicationBlogerModal";
import BloggerModal from "../AdminModal/Modals/BloggerModal/BloggerModal";
import ArrowLongIcon from "../kit/Icons/ArrowLongIcon";
import {history} from "../../store/configureStore";
import {connect} from "react-redux";
import SelectForm from "../kit/SelectForm/SelectForm";
import SettingsModal from "../AdminModal/Modals/Settings/SettingsModal";
import {changeSettingType} from "../../store/settings/settingsActions";
import {fetchLogoutUser} from "../../store/user/userActions";
import {ROUTE_ADVERTISE_PAGE, ROUTE_ADVERTISEMENT_BLOGGERS} from "../../global/Routes";

export const SETTINGS_TYPES = {
    1: "тип рекламы",
    2: "соц. сеть",
    3: "тег",
    4: "статус",
    5: "аудиторию"
}

const SETTINGS_OPTIONS = [
    {label: "Типы реклам", id: 1},
    {label: "Социальные сети", id: 2},
    {label: "Тематика", id: 3},
    {label: "Статусы", id: 4},
    {label: "Аудитория", id: 5},
]

const AdminHeader = ({title, type, advertise, changeSettingType, fetchLogoutUser, user, ...props}) => {
    const [open, setOpen] = useState(false);
    const [isOpenModal, toggleModal] = useState(null);

    const [settingModal, toggleSettingsModal] = useState(false)
    const anchorRef = useRef(null);

    const [settings, setSettingValue] = useState(1);

    function openApplicationModal(variant) {
        toggleModal(variant)
    }

    function handleTypeChange(e) {
        setSettingValue(e.target.value)
        changeSettingType(e.target.value)
    }

    const prevOpen = useRef(open);
    React.useEffect(() => {
        if (prevOpen.current === true && open === false) {
            anchorRef.current.focus();
        }

        prevOpen.current = open;
    }, [open]);


    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }

        setOpen(false);
    };

    function goBack() {
        if (type === 6) {
            history.push(ROUTE_ADVERTISE_PAGE + `/${props.match.params.advertisementId}`)
        } else {
            history.goBack()
        }
    }

    function redirectChooseBlogger() {
        history.push(ROUTE_ADVERTISEMENT_BLOGGERS + `/${props.match.params.advertisementId}`)
    }

    return (
        <>
            <div className="admin__header">
                <h1>
                    {
                        (type === 7 || type === 9 || type === 6) &&
                        <ArrowLongIcon onClick={goBack} className="go-back-icon"/>
                    }
                    {(type === 6 || type === 9) ? advertise && advertise.campaign_name : title}
                    {(type === 6 || type === 9) && (
                        <Button
                            variant="contained"
                            className="admin__header-buttons-group-create ml-20"
                            onClick={redirectChooseBlogger}>
                            добавить блогера
                        </Button>
                    )}
                </h1>
                <div className="admin__header-buttons-group">
                    {(type === 2 || type === 3 || type === 5) &&
                    <Button
                        variant="contained"
                        className="admin__header-buttons-group-create"
                        onClick={() => openApplicationModal("default")}>
                        {type === 5 ? "добавить блогера" : "создать заявку"}
                    </Button>
                    }
                    {type === 10 &&
                    <>
                        <SelectForm
                            options={SETTINGS_OPTIONS}
                            value={settings}
                            className="setting-options"
                            handleChange={handleTypeChange}/>
                        <Button
                            variant="contained"
                            className="admin__header-buttons-group-create"
                            onClick={() => toggleSettingsModal(true)}>
                            {`Создать ${SETTINGS_TYPES[settings]}`}
                        </Button>
                    </>
                    }

                    {type === 1 && (
                        <>
                            <Button
                                variant="contained"
                                className="admin__header-buttons-group-create --mr-20"
                                onClick={() => openApplicationModal("company")}>
                                Заявка компании
                            </Button>
                            <Button
                                variant="contained"
                                className="admin__header-buttons-group-create"
                                onClick={() => openApplicationModal("blogger")}>
                                Заявка блогера
                            </Button>
                        </>

                    )}
                </div>
                <div className="admin__header-menu">
                    {user && (
                        <div className="admin__header-names">
                            <p className="admin__header-name">{user.username}</p>
                            <p className="admin__header-position">Администратор</p>
                        </div>
                    )}
                    <Avatar alt="Admin" src={logo} className="admin__header-menu-avatar"/>
                    <div className="admin__header-menu-btn">
                        <Button
                            ref={anchorRef}
                            aria-controls={open ? 'menu-list-grow' : undefined}
                            aria-haspopup="true"
                            onClick={handleToggle}
                            className="admin__header-menu-btn"
                        >
                            <ArrowSimpleIcon/>
                        </Button>
                        <Popper
                            open={open}
                            anchorEl={anchorRef.current}
                            role={undefined}
                            className="admin__header__popover"
                            transition disablePortal>
                            {({TransitionProps, placement}) => (
                                <Grow
                                    {...TransitionProps}
                                    style={{
                                        transformOrigin: placement === 'bottom' ?
                                            'center top' :
                                            'center bottom'
                                    }}
                                >
                                    <Paper onClick={fetchLogoutUser}>
                                        <ClickAwayListener onClickAway={handleClose}>
                                            <MenuList autoFocusItem={open} id="menu-list-grow">
                                                <MenuItem>
                                                    <PowerIcon className="power-icon"/> Выйти
                                                </MenuItem>
                                            </MenuList>
                                        </ClickAwayListener>
                                    </Paper>
                                </Grow>
                            )}
                        </Popper>
                    </div>
                </div>
            </div>


            {!!isOpenModal &&
            <>
                {(type === 2 || isOpenModal === "company") &&
                <ApplicationCompanyModal onClose={() => toggleModal(null)}/>}
                {(type === 3 || isOpenModal === "blogger") &&
                <ApplicationBlogerModal onClose={() => toggleModal(null)}/>}
                {type === 5 && <BloggerModal onClose={() => toggleModal(null)}/>}
            </>
            }

            {settingModal &&
            <SettingsModal
                onClose={() => toggleSettingsModal(false)}
                type="create"
                typeId={settings}
            />}
        </>
    );
};

const mapStateToProps = state => ({
    advertise: state.advertisement.advertise,
    user: state.user.user
})

const mapDispatchToProps = dispatch => ({
    changeSettingType: typeId => dispatch(changeSettingType(typeId)),
    fetchLogoutUser: () => dispatch(fetchLogoutUser())
})

export default connect(mapStateToProps, mapDispatchToProps)(AdminHeader);