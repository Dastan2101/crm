import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

import './index.scss';
import CloseIcon from "../kit/Icons/CloseIcon";
import {Scrollbars} from 'react-custom-scrollbars';
import BloggersForm from "./BloggersForm";
import CompanyForm from "./CompanyForm";

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: "40px 0 50px",
    }
}));

const LandingModal = ({isOpen, onClose, type = "blogger", video}) => {
    const classes = useStyles();

    const videoLink = video.split("/")

    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={[classes.modal, "landing-modal"].join(" ")}
            open={!!isOpen}
            onClose={onClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
        >
            <Fade in={!!isOpen}>
                {type !== "video" ?
                    <div className={[classes.paper, "landing-modal__paper"].join(" ")}>
                        <h2 className="landing-modal__title">Отправить заявку
                            <span onClick={onClose}><CloseIcon className="close-icon"/></span></h2>
                        <Scrollbars style={{width: "100%", height: 500}}>
                            <>
                                {type === "blogger" ?
                                    <BloggersForm onClose={onClose}/>
                                    :
                                    <CompanyForm onClose={onClose}/>
                                }
                            </>
                        </Scrollbars>
                    </div>
                    :
                    <iframe width="75%"
                            height="50%"
                            src={`https://www.youtube.com/embed/${videoLink[3]}`}
                            frameBorder="0"
                            allow="accelerometer;
                             autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen/>
                }
            </Fade>

        </Modal>
    );
};

export default LandingModal;