import React, {useState} from 'react';
import InputForm from "../kit/InputForm/InputForm";
import InputPhoneMask from "../kit/InputPhoneMask/InputPhoneMask";
import DatePickerForm from "../kit/DatePickerForm/DatePickerForm";
import GeneralButton from "../kit/GeneralButton/GeneralButton";
import {Formik} from "formik";
import {dateFormatter} from "../../global/helpers";
import {
    fetchCreateApplicationCompany,
    fetchEditApplicationCompany
} from "../../store/applications/applicationsActions";
import {connect} from "react-redux";

const CompanyForm = (
    {
        onClose,
        fetchCreateApplicationCompany,
        initValues,
    }) => {

    const [state, setState] = useState({
        name: initValues ? initValues.name : "",
        company_name: initValues ? initValues.company_name : "",
        phone_number: initValues ? initValues.phone_number : "",
        deadline: initValues ? new Date(initValues.deadline) : new Date(),
        text: initValues ? initValues.text : "",
    })

    function dateHandleChange(date) {
        setState(prevState => {
            return {...prevState, deadline: date}
        })
    }

    function onSubmit(values) {
        const data = {...values};
        data.deadline = dateFormatter(state.deadline);
        fetchCreateApplicationCompany(data)
        onClose()
    }

    function validateForm(values) {
        const errors = {};
        if (!values.name) {
            errors.name = 'Обязательное поле';
        }
        if (!values.phone_number) {
            errors.phone_number = 'Обязательное поле';
        }
        if (!values.company_name) {
            errors.company_name = 'Обязательное поле';
        }
        if (!values.text) {
            errors.text = 'Обязательное поле';
        }
        return errors;
    }

    return (
        <Formik
            initialValues={state}
            validate={values => validateForm(values)}
            onSubmit={(values) => {
                onSubmit(values)
            }}
        >
            {
                ({
                     values,
                     errors,
                     touched,
                     handleChange,
                     dirty,
                     handleSubmit,
                 }) => (
                    <form className="modal-form" onSubmit={handleSubmit}>
                        <InputForm
                            label="Компания"
                            placeholder="Названия компании"
                            propertyName="company_name"
                            value={values.company_name}
                            onChange={handleChange}
                            isError={errors.company_name && touched.company_name && errors.company_name}
                            helperText={errors.company_name && touched.company_name && errors.company_name}
                        />
                        <InputForm
                            label="ФИО"
                            propertyName="name"
                            value={values.name}
                            onChange={handleChange}
                            placeholder="Введите ФИО"
                            isError={errors.name && touched.name && errors.name}
                            helperText={errors.name && touched.name && errors.name}
                        />
                        <InputPhoneMask
                            label="Телефон"
                            propertyName="phone_number"
                            value={values.phone_number}
                            onChange={handleChange}
                            isError={errors.phone_number && touched.phone_number && errors.phone_number}
                            helperText={errors.phone_number && touched.phone_number && errors.phone_number}/>
                        <div className="modal-form__form-datetime">
                            <p>Когда нам начать работать?</p>
                            <DatePickerForm
                                className="modal-form__form-datetime-calendar"
                                selected={state.deadline}
                                onChange={dateHandleChange}
                            />
                        </div>
                        <InputForm
                            label="описание ТЗ *"
                            placeholder="Опишите что вам нужно"
                            value={values.text}
                            propertyName="text"
                            onChange={handleChange}
                            multiline
                            rows={4}
                            isError={errors.text && touched.text && errors.text}
                            helperText={errors.text && touched.text && errors.text}
                        />
                        <GeneralButton
                            type="submit"
                            className="modal-form__btn"
                            variant="outlined">
                            Сохранить
                        </GeneralButton>
                    </form>
                )
            }
        </Formik>
    );
};
const mapDispatchToProps = dispatch => ({
    fetchCreateApplicationCompany: data => dispatch(fetchCreateApplicationCompany(data)),
    fetchEditApplicationCompany: (id, data) => dispatch(fetchEditApplicationCompany(id, data))
})

export default connect(null, mapDispatchToProps)(CompanyForm);
