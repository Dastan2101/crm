import React, {useState} from 'react';
import './index.scss';
import SortIcon from "../Icons/SortIcon";
import Menu from "@material-ui/core/Menu";
import Fade from "@material-ui/core/Fade";
import MenuItem from "@material-ui/core/MenuItem";

const SortSelect = ({options, onChange, sortActiveValue}) => {
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    function onSelect(option) {
        onChange(option)
        handleClose()
    }

    return (
        <div className="sort-select">
            <button onClick={handleClick}>{sortActiveValue ? sortActiveValue.label : "Сортировать"}</button>
            <SortIcon/>
            <Menu
                id="fade-menu"
                anchorEl={anchorEl}
                keepMounted
                open={open}
                onClose={handleClose}
                TransitionComponent={Fade}
            >
                {options.map((option, ndx) => (
                    <MenuItem key={ndx} onClick={() => onSelect(option)}>{option.label}</MenuItem>
                ))}
            </Menu>
        </div>
    );
};

export default SortSelect;