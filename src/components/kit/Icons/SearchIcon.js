import React from 'react';

const SearchIcon = (
    {
        width = 16,
        height = 16,
        color = '#A6ACBE',
        className,
        onClick
    }) => {
    return (
        <span style={{width: width, height: height}} onClick={onClick} className={className}>
            <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 16 16" fill="none">
<path fillRule="evenodd" clipRule="evenodd"
      d="M7.33337 2.66671C4.75605 2.66671 2.66671 4.75605 2.66671 7.33337C2.66671 9.9107 4.75605 12 7.33337 12C9.9107 12 12 9.9107 12 7.33337C12 4.75605 9.9107 2.66671 7.33337 2.66671ZM1.33337 7.33337C1.33337 4.01967 4.01967 1.33337 7.33337 1.33337C10.6471 1.33337 13.3334 4.01967 13.3334 7.33337C13.3334 10.6471 10.6471 13.3334 7.33337 13.3334C4.01967 13.3334 1.33337 10.6471 1.33337 7.33337Z"
      fill={color}/>
<path fillRule="evenodd" clipRule="evenodd"
      d="M10.6286 10.6286C10.889 10.3683 11.3111 10.3683 11.5714 10.6286L14.4714 13.5286C14.7318 13.789 14.7318 14.2111 14.4714 14.4714C14.2111 14.7318 13.789 14.7318 13.5286 14.4714L10.6286 11.5714C10.3683 11.3111 10.3683 10.889 10.6286 10.6286Z"
      fill={color}/>
</svg>
        </span>
    );
};

export default SearchIcon;