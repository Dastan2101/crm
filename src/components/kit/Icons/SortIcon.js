import React from 'react';

const SortIcon = (
    {
        width = 20,
        height = 20,
        color = '#C9CED6',
        className,
        onClick
    }) => {
    return (
        <span style={{width: width, height: height}} onClick={onClick} className={className}>
            <svg xmlns="http://www.w3.org/2000/svg" width={width} height={width} viewBox="0 0 20 20" fill="none">
<path fillRule="evenodd" clipRule="evenodd"
      d="M0.910787 2.14923C1.04734 1.85492 1.34228 1.66663 1.66672 1.66663H18.3334C18.6578 1.66663 18.9528 1.85492 19.0893 2.14923C19.2259 2.44353 19.1792 2.79033 18.9697 3.03806L12.5001 10.6884V17.5C12.5001 17.7888 12.3505 18.057 12.1048 18.2088C11.8591 18.3607 11.5524 18.3745 11.294 18.2453L7.96071 16.5786C7.67839 16.4375 7.50005 16.1489 7.50005 15.8333V10.6884L1.03041 3.03806C0.820912 2.79033 0.774238 2.44353 0.910787 2.14923ZM3.4628 3.33329L8.96969 9.84519C9.09691 9.99563 9.16672 10.1863 9.16672 10.3833V15.3183L10.8334 16.1516V10.3833C10.8334 10.1863 10.9032 9.99563 11.0304 9.84519L16.5373 3.33329H3.4628Z"
      fill={color}/>
</svg>
        </span>
    );
};

export default SortIcon;