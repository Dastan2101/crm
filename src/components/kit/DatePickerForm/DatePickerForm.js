import React from 'react';
import DatePicker from "react-datepicker";
import './index.scss';
import { registerLocale } from  "react-datepicker";
import ru from 'date-fns/locale/ru';
import CalendarIcon from "../Icons/CalendarIcon";
registerLocale('ru', ru)

const DatePickerForm = ({className, placeholder, onChange, selected, ...props}) => {

    return (
        <div className={["admin-date-picker-root", className].join(" ")}>
            <DatePicker
                dateFormat="dd-MM-yyyy"
                selected={selected}
                onChange={onChange}
                className="admin-date-picker-root__date-field"
                locale="ru"
                placeholderText={placeholder}
                {...props}
            />
            <CalendarIcon/>
        </div>
    );
};

export default DatePickerForm;