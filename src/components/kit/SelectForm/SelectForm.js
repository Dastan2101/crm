import React from 'react';
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import {makeStyles} from "@material-ui/core/styles";

import './index.scss';
import FormLabel from "@material-ui/core/FormLabel";

const useStyles = makeStyles(theme => ({
    root: {
        borderRadius: '6px',
        color: "#110421",
        margin: "10px 0",
        width: '100%',
        backgroundColor: '#ffffff',
        padding: '12px 0',
    },
    option: {
        padding: '0 16px'
    }
}));
const SelectForm = ({value, handleChange, options, defaultValue, className, propertyName, label}) => {
    const classes = useStyles();
    return (
        <FormControl className={[classes.root, "input-form-select", className].join(' ')}>
            <FormLabel component="legend">{label}</FormLabel>
            <TextField
                select
                value={value}
                onChange={handleChange}
                name={propertyName}
            >
                {defaultValue &&
                <MenuItem className="input-form-select__option" key={defaultValue.id} value={defaultValue.id}>
                    {defaultValue.label}
                </MenuItem>
                }
                {options &&
                options.map(option => (
                    <MenuItem key={option.id} value={option.id}>
                        {option.label}
                    </MenuItem>
                ))
                }
            </TextField>
        </FormControl>
    );
};

export default SelectForm;