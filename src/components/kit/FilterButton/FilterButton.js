import React from 'react';
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";
import Popper from "@material-ui/core/Popper";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import MenuList from "@material-ui/core/MenuList";
import MenuItem from "@material-ui/core/MenuItem";
import './index.scss';
import CheckIcon from "../Icons/CheckIcon";
import ArrowSimpleIcon from "../Icons/ArrowSimpleIcon";

const FilterButton = ({options, label = "Выбрать", icon, onClick, type}) => {
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);
    const [selectedIndex, setSelectedIndex] = React.useState("");

    const handleMenuItemClick = (event, index) => {
        setSelectedIndex(index);
        const data = {...options[index]};
        data.type = type
        onClick(data)
        setOpen(false);
    };

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }

        setOpen(false);
    };

    return (
        <div className="filter-button">
            <ButtonGroup variant="contained" color="primary" ref={anchorRef} aria-label="split button"
                         onClick={handleToggle}>
                <Button>
                    <span className="filter-button__name">
                        <span className="filter-button__icon">
                            {icon}
                        </span>
                        {selectedIndex !== "" ?
                            <span>{options[selectedIndex] && options[selectedIndex].value}</span>
                            :
                            <span>{label}</span>
                        }
                    </span>
                </Button>
                <Button
                    color="primary"
                    size="small"
                    aria-controls={open ? 'split-button-menu' : undefined}
                    aria-expanded={open ? 'true' : undefined}
                    aria-label="select merge strategy"
                    aria-haspopup="menu"
                >
                    <ArrowSimpleIcon width={11} color="#6A7083"/>
                </Button>
            </ButtonGroup>
            <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                {({TransitionProps, placement}) => (
                    <Grow
                        {...TransitionProps}
                        style={{
                            transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
                        }}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={handleClose}>
                                <MenuList id="split-button-menu">
                                    <MenuItem
                                        selected={selectedIndex === ""}
                                        onClick={(event) => handleMenuItemClick(event, "")}
                                    >
                                        Все
                                    </MenuItem>
                                    {options.map((option, index) => (
                                        <MenuItem
                                            key={index}
                                            selected={index === selectedIndex}
                                            onClick={(event) => handleMenuItemClick(event, index)}
                                        >
                                            {option.label}
                                            {selectedIndex === index &&
                                            <CheckIcon
                                            className="checked"
                                            color="#7400FF"/>}
                                        </MenuItem>
                                    ))}
                                </MenuList>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </div>
    );
};

export default FilterButton;