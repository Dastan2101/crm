import React, {useEffect, useRef, useState} from 'react';
import Button from "@material-ui/core/Button";
import Popper from "@material-ui/core/Popper";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import MenuList from "@material-ui/core/MenuList";
import MenuItem from "@material-ui/core/MenuItem";

import './index.scss';
import ArrowSimpleIcon from "../Icons/ArrowSimpleIcon";

const CustomSelect = ({data, onChange, index, className, value}) => {

    const [open, setOpen] = useState(false);
    const [selectedItem, setItem] = useState({name: "Выбрать"})
    const anchorRef = useRef(null);

    useEffect(function () {
        if (value) {
            const findEl = data.find(it => it.id === value);
            setItem(findEl)
        }
    }, [value])

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }
        setOpen(false);
    };

    function handleListKeyDown(event) {
        if (event.key === 'Tab') {
            event.preventDefault();
            setOpen(false);
        }
    }

    function onSelect(item, e) {
        setItem(item);
        onChange(index, e, item.id)
        handleClose(e)
    }


    const prevOpen = useRef(open);
    useEffect(() => {
        if (prevOpen.current === true && open === false) {
            anchorRef.current.focus();
        }

        prevOpen.current = open;
    }, [open]);
    return (
        <div className={["social-select", className].join(" ")}>
            <Button
                ref={anchorRef}
                aria-controls={open ? 'menu-list-grow' : undefined}
                aria-haspopup="true"
                onClick={handleToggle}
            >
                <p>{selectedItem ? selectedItem.name : "Выбрать"}</p>
                <ArrowSimpleIcon width={9} height={5} color="#1A051D"/>
            </Button>
            <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                {({TransitionProps, placement}) => (
                    <Grow
                        {...TransitionProps}
                        style={{transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={handleClose}>
                                <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                                    {data && data.map(social => (
                                        <MenuItem key={social.id} onClick={(e) => onSelect(social, e)}>
                                            {social.name}
                                        </MenuItem>
                                    ))}
                                </MenuList>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </div>
    );
};

export default CustomSelect;