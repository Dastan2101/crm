import React, {useEffect, useState} from 'react';
import {Formik} from "formik";
import InputForm from "../../../kit/InputForm/InputForm";
import {isEmptyArray} from "../../../../global/helpers";
import CustomSelect from "../../../kit/SocialSelect/CustomSelect";
import GeneralButton from "../../../kit/GeneralButton/GeneralButton";
import {fetchSocialNetworks} from "../../../../store/applications/applicationsActions";
import {connect} from "react-redux";
import {fetchAudienceAges, fetchContentTypes} from "../../../../store/bloggers/bloggersActions";
import CommonSpinner from "../../../kit/CommonSpinner/CommonSpinner";
import TrashIcon from "../../../kit/Icons/TrashIcon";

const SocialsForm = (
    {
        onSubmit,
        fetchSocialNetworks,
        socialNetworks,
        initValues,
        fetchContentTypes,
        contentTypes,
        audienceAges,
        fetchAudienceAges,
        blogger,
    }) => {

    const [audience_age, setAge] = useState("")

    const [inputFields, setInputFields] = useState([
        {social_network: '', link: ''}
    ]);

    const [contentList, setContent] = useState([]);

    function toggleContent(id) {
        const copyState = [...contentList];
        if (!contentList.includes(id)) {
            copyState.push(id)
            setContent(copyState)
        } else {
            const index = contentList.findIndex(it => it === id);
            copyState.splice(index, 1)
            setContent(copyState)
        }
    }

    useEffect(function () {
        fetchSocialNetworks()
        fetchContentTypes()
        fetchAudienceAges()
        if (blogger) {
            const initVal = [];
            blogger.social_network && blogger.social_network.forEach(item => {
                initVal.push({social_network: item.social_network, link: item.link, id: item.id});
            })
            setAge(blogger.audience_age);
            const contents = []
            blogger.subject_list && blogger.subject_list.forEach(content => {
                contents.push(content.id)
            })
            if (!isEmptyArray(initVal)) {
                setInputFields(initVal);
            }
            if (!isEmptyArray(contents)) {
                setContent(contents)
            }
        } else {
            setInputFields(initValues.social_network)
            setContent(initValues.subject)
            setAge(initValues.audience_age)
        }
    }, [])


    const handleInputChange = (index, event, item) => {
        const values = [...inputFields];
        if (event.target.name === "link") {
            values[index].link = event.target.value;
        } else {
            values[index].social_network = item;
        }

        setInputFields(values);
    };

    function handleAddFields() {
        const values = [...inputFields];
        values.push({social_network: '', link: ''});
        setInputFields(values);
    }

    function handleRemoveField(ndx) {
        const values = [...inputFields];
        values.splice(ndx, 1)
        setInputFields(values);
    }

    function audienceHandler(e, ndx, id) {
        setAge(id)
    }

    function validateForm(values) {
        const errors = {};
        if (!audience_age) {
            errors.audience_age = 'Обязательное поле';
        }
        if (!contentList.length) {
            errors.contentList = 'Обязательное поле';
        }
        inputFields.forEach((field, ndx) => {
            if (!inputFields[ndx].social_network || !inputFields[ndx].link) {
                errors.inputFields = 'Обязательное поле';
            }
        })
        return errors;
    }

    function onSubmitting() {

        const data = {
            audience_age,
            social_network: inputFields,
            subject: contentList
        }
        onSubmit(data)
    }

    const changeData = [];
    audienceAges && audienceAges.forEach(item => changeData.push({name: item.audience_age, id: item.id}))

    return (
        <Formik
            initialValues={audience_age}
            validate={values => validateForm(values)}
            onSubmit={onSubmitting}
        >
            {
                ({
                     values,
                     errors,
                     touched,
                     handleChange,
                     dirty,
                     handleSubmit,
                 }) => (
                    <form className="modal-form" onSubmit={handleSubmit}>
                        {socialNetworks &&
                        !isEmptyArray(socialNetworks) &&
                        <div className="modal-form__socials">
                            <p style={
                                {
                                    color: errors.inputFields ? "#FA445C" : "#1A051D"
                                }
                            }>соц.сеть *</p>
                            {inputFields.map((field, ndx) => (
                                <div className="modal-form__socials-fields" key={ndx}>
                                    <CustomSelect
                                        data={socialNetworks}
                                        index={ndx}
                                        value={field.social_network}
                                        onChange={handleInputChange}
                                    />
                                    <InputForm
                                        propertyName="link"
                                        value={field.link}
                                        onChange={event => handleInputChange(ndx, event)}
                                        placeholder="Ссылка на соц.сеть"
                                    />
                                    {ndx !== 0 && <TrashIcon
                                        className="field_remove"
                                        onClick={() => handleRemoveField(ndx)}/>}
                                </div>
                            ))}
                            {socialNetworks &&
                            <span onClick={handleAddFields} className="modal-form__socials-add">
                                    Добавить еще
                                </span>}
                        </div>
                        }
                        {errors.inputFields &&
                        <span className="error-text">{errors.inputFields}</span>
                        }
                        <div className="modal-form__content_types">
                            <p style={
                                {
                                    color: errors.contentList ? "#FA445C" : "#1A051D"
                                }
                            }>тематика (теги) *</p>
                            <div className="modal-form__content_types-list">
                                {contentTypes ? contentTypes.map(content => (
                                    <div
                                        key={content.id}
                                        className={`content-type 
                                        ${contentList.includes(content.id) && "content-type__active"}`}
                                        onClick={() => toggleContent(content.id)}>
                                    <span className="content-type__name">
                                        {content.subject_name}
                                    </span>
                                    </div>
                                )) : <CommonSpinner centered isLoading={!contentTypes}/>}
                            </div>
                            {errors.contentList &&
                            <span className="error-text">{errors.contentList}</span>
                            }
                            <div className="modal-form__status-select">
                                <p style={
                                    {
                                        color: errors.audience_age ? "#FA445C" : "#1A051D"
                                    }
                                } className="audience_age">Аудитория</p>
                                {audienceAges && !isEmptyArray(audienceAges) &&
                                <CustomSelect
                                    className="select"
                                    data={changeData}
                                    value={audience_age}
                                    onChange={audienceHandler}
                                />}
                                {!audienceAges && <CommonSpinner centered isLoading={true}/>}
                                {errors.audience_age &&
                                <span className="error-text">{errors.audience_age}</span>
                                }
                            </div>
                        </div>

                        <GeneralButton
                            type="submit"
                            className="modal-form__btn"
                            variant="outlined">
                            Сохранить
                        </GeneralButton>
                    </form>
                )
            }
        </Formik>
    );
};

const mapStateToProps = state => ({
    socialNetworks: state.applicationsInfo.socialNetworks,
    contentTypes: state.bloggers.contentTypes,
    audienceAges: state.bloggers.audienceAges,
    blogger: state.bloggers.blogger
})

const mapDispatchToProps = dispatch => ({
    fetchSocialNetworks: () => dispatch(fetchSocialNetworks()),
    fetchContentTypes: () => dispatch(fetchContentTypes()),
    fetchAudienceAges: () => dispatch(fetchAudienceAges())
})

export default connect(mapStateToProps, mapDispatchToProps)(SocialsForm);