import React, {useEffect, useState} from 'react';
import {isEmptyArray} from "../../../../global/helpers";
import CustomSelect from "../../../kit/SocialSelect/CustomSelect";
import InputForm from "../../../kit/InputForm/InputForm";
import TrashIcon from "../../../kit/Icons/TrashIcon";
import CommonSpinner from "../../../kit/CommonSpinner/CommonSpinner";
import GeneralButton from "../../../kit/GeneralButton/GeneralButton";
import {Formik} from "formik";
import {connect} from "react-redux";
import {fetchAdvertTypes} from "../../../../store/bloggers/bloggersActions";

const AdvertisementForm = ({onSubmit, fetchAdvertTypes, advertTypes, blogger}) => {

    useEffect(function () {
        fetchAdvertTypes()
    }, [])

    const [inputFields, setInputFields] = useState([
        {advertisement_type: '', price: 0}
    ]);

    useEffect(function () {
        if (blogger) {
            const services = []
            blogger.service.forEach(item => {
                services.push({advertisement_type: item.advertisement_type, price: item.price})
            })
            if (!isEmptyArray(services)) {
                setInputFields(services)
            }
        }
    }, [])

    const handleInputChange = (index, event, item) => {
        const values = [...inputFields];
        if (event.target.name === "price") {
            values[index].price = Math.abs(event.target.value);
        } else {
            values[index].advertisement_type = item;
        }

        setInputFields(values);
    };

    function handleAddFields() {
        const values = [...inputFields];
        values.push({advertisement_type: '', price: 0});
        setInputFields(values);
    }

    function handleRemoveField(ndx) {
        const values = [...inputFields];
        values.splice(ndx, 1)
        setInputFields(values);
    }

    function validateForm(values) {
        const errors = {};

        inputFields.forEach((field, ndx) => {
            if (!inputFields[ndx].advertisement_type) {
                errors.inputFields = 'Обязательное поле';
            }
        })
        return errors;
    }

    function onSubmitting() {
        const data = {
            service: inputFields
        }
        onSubmit(data)
    }

    const changeData = [];
    advertTypes && advertTypes.forEach(item => changeData.push({name: item.type_name, id: item.id}))

    return (
        <Formik
            initialValues={{}}
            validate={values => validateForm(values)}
            onSubmit={onSubmitting}
        >
            {
                ({
                     values,
                     errors,
                     touched,
                     handleChange,
                     dirty,
                     handleSubmit,
                 }) => (
                    <form className="modal-form" onSubmit={handleSubmit}>
                        <div className="modal-form__socials">
                            <p style={
                                {
                                    color: errors.inputFields ? "#FA445C" : "#1A051D"
                                }
                            }>вид рекламы *</p>
                            {advertTypes &&
                            !isEmptyArray(advertTypes) &&
                            inputFields.map((field, ndx) => (
                                <div className="modal-form__socials-fields" key={ndx}>
                                    <CustomSelect
                                        data={changeData}
                                        index={ndx}
                                        value={field.advertisement_type}
                                        onChange={handleInputChange}
                                    />
                                    <InputForm
                                        propertyName="price"
                                        value={field.price ? Math.abs(field.price) : 0}
                                        type="number"
                                        onChange={event => handleInputChange(ndx, event)}
                                        placeholder="Цена"
                                    />
                                    {ndx !== 0 && <TrashIcon
                                        className="field_remove"
                                        onClick={() => handleRemoveField(ndx)}/>}
                                </div>
                            ))}
                            <span onClick={handleAddFields} className="modal-form__socials-add">
                                    Добавить еще
                            </span>
                            {errors.inputFields &&
                            <span className="error-text">{errors.inputFields}</span>
                            }
                        </div>

                        {!advertTypes && <CommonSpinner isLoading={!advertTypes} centered/>}

                        <GeneralButton
                            type="submit"
                            className="modal-form__btn"
                            variant="outlined">
                            Сохранить
                        </GeneralButton>
                    </form>
                )
            }
        </Formik>
    );
};


const mapStateToProps = state => ({
    advertTypes: state.bloggers.advertTypes,
    blogger: state.bloggers.blogger
})

const mapDispatchToProps = dispatch => ({
    fetchAdvertTypes: () => dispatch(fetchAdvertTypes())
})

export default connect(mapStateToProps, mapDispatchToProps)(AdvertisementForm);