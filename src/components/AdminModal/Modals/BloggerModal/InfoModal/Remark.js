import React, {useState} from 'react';
import InputForm from "../../../../kit/InputForm/InputForm";
import GeneralButton from "../../../../kit/GeneralButton/GeneralButton";
import {createBloggerRemark} from "../../../../../store/bloggers/bloggersActions";
import {connect} from "react-redux";
import EditIcon from "../../../../kit/Icons/EditIcon";

const Remark = ({data, createBloggerRemark}) => {
    const [remark_text, setRemark] = useState(data.remark_text || "");
    const [isEditable, toggle] = useState(false)

    function createRemark() {
        createBloggerRemark(data.id, {remark_text});
        toggle(false)
    }

    return (
        <div className="blogger-info__remarks">
            <h6><span>Примечания</span>
                {data.remark_text &&
                <span className="edit"
                      onClick={() => toggle(!isEditable)}>
                    <EditIcon/> Редактировать
                </span>
                }
            </h6>
            {data.remark_text && !isEditable ?
                <p className="remark_text">
                    {data.remark_text}
                </p>
                :
                <InputForm
                    propertyName="remark"
                    multiline
                    value={remark_text}
                    rows={4}
                    onChange={e => setRemark(e.target.value)}
                />}
            {(!data.remark_text || isEditable) &&
            <GeneralButton disabled={!remark_text} onSubmit={createRemark}>
                Сохранить
            </GeneralButton>}
        </div>
    );
};
const mapDispatchToProps = dispatch => ({
    createBloggerRemark: (id, text) => dispatch(createBloggerRemark(id, text))
})


export default connect(null, mapDispatchToProps)(Remark);