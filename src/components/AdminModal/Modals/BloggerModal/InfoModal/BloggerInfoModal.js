import React, {useEffect, useState} from 'react';
import AdminModal from "../../../AdminModal";
import '../index.scss';
import {GENDER_TYPES} from "../../../../../global/constants";
import PersonalInfo from "./PersonalInfo";
import StatisticInfo from "./StatisticInfo";
import PriceInfo from "./PriceInfo";
import Remark from "./Remark";
import {clearBloggerInfo, fetchBloggerById} from "../../../../../store/bloggers/bloggersActions";
import {connect} from "react-redux";
import CommonSpinner from "../../../../kit/CommonSpinner/CommonSpinner";

import avatar from '../../../../../assets/logo_icon.png'

const TABS = ["Личное", "Статистика", "Цены", "Примечания"];

const BloggerInfoModal = ({onClose, data, fetchBloggerById, blogger, clearBloggerInfo}) => {
    const year = data.birthday_date && new Date(data.birthday_date);
    const age = year && new Date().getFullYear() - year.getFullYear();

    const [selectedIndex, setIndex] = useState(0);

    useEffect(function () {
        fetchBloggerById(data.id)
        return function () {
            clearBloggerInfo()
        }
    }, [])

    return (
        <AdminModal onClose={onClose} width="600px">
            <div className="blogger-info">
                <div className="blogger-info__header">
                    <div className="blogger-info__header-image">
                        <img src={data.image || avatar} alt="img"/>
                        <span>{data.er}</span>
                    </div>
                    <p className="blogger-info__header-status-name">
                        {data.status_name}
                    </p>
                    <h5 className="blogger-info__header-names">
                        {data.name &&
                        <span className="name">{data.name.charAt(0)}.</span>}
                        {data.middle_name &&
                        <span className="middle-name">{data.middle_name.charAt(0)}.</span>}
                        <span className="last-name">
                            {data.surname}</span>
                        {!isNaN(Number(age)) &&
                        <span className="age">, {age}</span>}
                    </h5>
                    <p className="blogger-info__header-country">
                        {data.gender && GENDER_TYPES[data.gender]},
                        <span> {data.country_name}</span>
                    </p>
                </div>
                <div className="blogger-info__socials">
                    {data.social_network && data.social_network.map(social => (
                        <div className="blogger-info__social" key={social.id}>
                            <img src={social.social_network_image} alt="#"/>
                            <div className="blogger-info__social-subs">
                                <p className="count">{social.subscribers || 0}</p>
                                <a href={social.soc_network_url} rel="noopener noreferrer" target="_blank" className="link">
                                    {social.link}
                                </a>
                            </div>
                        </div>
                    ))}
                </div>
                <div className="blogger-info__services">
                    {data.subject_list && data.subject_list.map(subject => (
                        <div className="blogger-info__service" key={subject.id} style={{backgroundColor: subject.back_color}}>
                            <span style={{color: subject.color}}>{subject.subject_name}</span>
                        </div>
                    ))}
                </div>

                <div className="blogger-info__tabs">
                    {TABS.map((name, ndx) => (
                        <p className={`blogger-info__tab-name 
                        ${selectedIndex === ndx && "blogger-info__tab-name--active"}`}
                           key={ndx}
                           onClick={() => setIndex(ndx)}>
                            {name}
                        </p>
                    ))}
                </div>
                {!blogger ? <CommonSpinner isLoading={!blogger} centered/>
                    :
                    <>
                        {selectedIndex === 0 && <PersonalInfo data={blogger}/>}
                        {selectedIndex === 1 && <StatisticInfo data={blogger}/>}
                        {selectedIndex === 2 && <PriceInfo data={blogger}/>}
                        {selectedIndex === 3 && <Remark data={blogger}/>}
                    </>
                }

            </div>
        </AdminModal>
    );
};

const mapStateToProps = state => ({
    blogger: state.bloggers.blogger,
})

const mapDispatchToProps = dispatch => ({
    fetchBloggerById: id => dispatch(fetchBloggerById(id)),
    clearBloggerInfo: () => dispatch(clearBloggerInfo())
})

export default connect(mapStateToProps, mapDispatchToProps)(BloggerInfoModal);