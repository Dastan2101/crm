import React, {useEffect, useState} from 'react';
import AdminModal from "../../AdminModal";
import {Formik} from "formik";
import InputForm from "../../../kit/InputForm/InputForm";
import GeneralButton from "../../../kit/GeneralButton/GeneralButton";
import {SETTINGS_TYPES} from "../../../AdminHeader/AdminHeader";
import {
    fetchContentType,
    fetchCreateAdvertType, fetchCreateAudienceAge, fetchCreateContentType,
    fetchCreateSocialNetwork, fetchCreateStatus,
    fetchEditAdvertType, fetchEditAudienceAge, fetchEditSocialNetwork, fetchEditStatus
} from "../../../../store/settings/settingsActions";
import {connect} from "react-redux";
import DownloadIcon from "../../../kit/Icons/DownloadIcon";
import {TAG_COLOR_VARIANTS} from "../../../../global/constants";
import CheckIcon from "../../../kit/Icons/CheckIcon";

export const AdvertisementType = 1;
export const SOCIAL_NETWORK = 2;
export const CONTENT_TYPE = 3;
export const STATUSES_TYPE = 4;
export const AUDIENCE_TYPE = 5;

const SettingsModal = (
    {
        onClose,
        type,
        typeId,
        initValues,
        fetchCreateAdvertType,
        fetchEditAdvertType,

        fetchCreateSocialNetwork,
        fetchEditSocialNetwork,

        fetchCreateContentType,
        fetchContentType,

        fetchCreateStatus,
        fetchEditStatus,

        fetchCreateAudienceAge,
        fetchEditAudienceAge
    }) => {

    const [state, setState] = useState({
        type_name: initValues ? initValues.type_name : "",
        audience_age: initValues ? initValues.audience_age : "",
        subject_name: initValues ? initValues.subject_name : "",
        back_color: initValues ? initValues.back_color : "#3DD5FF",
        color: initValues ? initValues.color : "#FFFFFF",
        name: initValues ? initValues.name : "",
        root_url: initValues ? initValues.root_url : "",
        image: "",
    })

    function validateForm(values) {
        const errors = {};
        if (typeId === AdvertisementType) {
            if (!values.type_name) {
                errors.type_name = 'Обязательное поле';
            }
        }
        if (typeId === AUDIENCE_TYPE) {
            if (!values.audience_age) {
                errors.audience_age = 'Обязательное поле';
            } else if (values.audience_age.length > 32) {
                errors.audience_age = 'Максимальная длина символов 32';
            }
        }
        if (typeId === SOCIAL_NETWORK || typeId === STATUSES_TYPE) {
            if (!values.name) {
                errors.name = 'Обязательное поле';
            }
            if (!values.root_url) {
                errors.root_url = 'Обязательное поле';
            }
        }
        if (typeId === CONTENT_TYPE) {
            if (!values.subject_name) {
                errors.subject_name = 'Обязательное поле';
            }
        }

        return errors;
    }

    function fileHandler(e) {
        e.persist()
        if (e.target.files && e.target.files[0]) {
            setState(prevState => {
                return {...prevState, image: e.target.files[0]}
            })
        }
    }

    function colorHandler(type, value) {
        setState(prevState => {
            return {...prevState, [type]: value}
        })
    }

    function onSubmit(values) {
        if (typeId === AdvertisementType) {
            const data = {
                type_name: values.type_name
            }
            if (type === "create") {
                fetchCreateAdvertType(data);
            } else {
                fetchEditAdvertType(data, initValues.id)
            }
        }

        if (typeId === AUDIENCE_TYPE) {
            const data = {
                audience_age: values.audience_age
            }
            if (type === "create") {
                fetchCreateAudienceAge(data);
            } else {
                fetchEditAudienceAge(data, initValues.id)
            }
        }

        if (typeId === SOCIAL_NETWORK) {

            const formData = new FormData();
            formData.append("name", values.name)
            formData.append("root_url", values.root_url)

            if (state.image) {
                formData.append("image", state.image)
            }
            if (type === "create") {
                fetchCreateSocialNetwork(formData)
            } else {
                fetchEditSocialNetwork(formData, initValues.id)
            }
        }

        if (typeId === CONTENT_TYPE) {
            const data = {
                back_color: state.back_color,
                color: state.color,
                subject_name: values.subject_name
            }
            if (type === "create") {
                fetchCreateContentType(data)
            } else {
                fetchContentType(data, initValues.id)
            }
        }

        if (typeId === STATUSES_TYPE) {
            const formData = new FormData();
            formData.append("name", values.name)

            if (state.image) {
                formData.append("icon", state.image)
            }
            if (type === "create") {
                fetchCreateStatus(formData)
            } else {
                fetchEditStatus(formData, initValues.id)
            }
        }


        onClose()

    }

    return (
        <AdminModal onClose={onClose} title={`Создать ${SETTINGS_TYPES[typeId]}`} application={initValues}
                    backdrop={false}>
            <Formik
                initialValues={state}
                validate={values => validateForm(values)}
                onSubmit={(values) => {
                    onSubmit(values)
                }}
            >
                {
                    ({
                         values,
                         errors,
                         touched,
                         handleChange,
                         dirty,
                         handleSubmit,
                     }) => (
                        <form className="modal-form" onSubmit={handleSubmit}>
                            {typeId === AdvertisementType &&
                            <InputForm
                                label="Наименование"
                                placeholder="Введите наименование"
                                propertyName="type_name"
                                value={values.type_name}
                                onChange={handleChange}
                                isError={errors.type_name && touched.type_name && errors.type_name}
                                helperText={errors.type_name && touched.type_name && errors.type_name}
                            />}
                            {typeId === AUDIENCE_TYPE &&
                            <InputForm
                                label="Наименование"
                                placeholder="от x до x лет"
                                propertyName="audience_age"
                                value={values.audience_age}
                                onChange={handleChange}
                                isError={errors.audience_age && touched.audience_age && errors.audience_age}
                                helperText={errors.audience_age && touched.audience_age && errors.audience_age}
                            />}
                            {typeId === CONTENT_TYPE &&
                            <>
                                <InputForm
                                    label="Название тега"
                                    placeholder="Введите название"
                                    propertyName="subject_name"
                                    value={values.subject_name}
                                    onChange={handleChange}
                                    isError={errors.subject_name && touched.subject_name && errors.subject_name}
                                    helperText={errors.subject_name && touched.subject_name && errors.subject_name}
                                />
                                <p className="tag-color-title">
                                    Выберите цвет фона
                                </p>
                                <div className="tag-colors">
                                    {TAG_COLOR_VARIANTS.map(color => (
                                        <span className="tag-colors__color"
                                              key={color}
                                              onClick={() => colorHandler("back_color", color)}
                                              style={{backgroundColor: color}}>
                                            {state.back_color === color && <CheckIcon width={16} height={16}/>}
                                        </span>
                                    ))}
                                </div>
                                <p className="tag-color-title">
                                    Выберите цвет текста
                                </p>
                                <div className="tag-colors">
                                    {TAG_COLOR_VARIANTS.map(color => (
                                        <span className="tag-colors__color"
                                              key={color}
                                              onClick={() => colorHandler("color", color)}
                                              style={{backgroundColor: color}}>
                                            {state.color === color && <CheckIcon width={16} height={16}/>}
                                        </span>
                                    ))}
                                </div>
                                {values.subject_name &&
                                <div className="preview-tag-root">
                                    <div className="preview-tag" style={{backgroundColor: state.back_color}}>
                                        <span style={{color: state.color}}>{values.subject_name}</span>
                                    </div>
                                </div>
                                }
                            </>
                            }
                            {(typeId === SOCIAL_NETWORK || typeId === STATUSES_TYPE) &&
                            <>
                                <p className="icon-choose-text">Иконка</p>
                                <div className="file-upload-wrapper"
                                     data-text={state.image ? "Иконка загружена" : "Выберите иконку"}>
                                    <input onChange={fileHandler}
                                           name="file-upload-field"
                                           type="file"
                                           className="file-upload-field"/>
                                    <DownloadIcon className="file-upload-wrapper__icon"/>
                                </div>
                                <InputForm
                                    label={typeId === SOCIAL_NETWORK ? "Название соц. сети" : "Название статуса"}
                                    placeholder="Введите наименование"
                                    propertyName="name"
                                    value={values.name}
                                    onChange={handleChange}
                                    isError={errors.name && touched.name && errors.name}
                                    helperText={errors.name && touched.name && errors.name}
                                />
                                {typeId === SOCIAL_NETWORK && (
                                    <InputForm
                                        label="Относительный адрес сервиса"
                                        placeholder="https://www.instagram.com/"
                                        propertyName="root_url"
                                        value={values.root_url}
                                        onChange={handleChange}
                                        isError={errors.root_url && touched.root_url && errors.root_url}
                                        helperText={errors.root_url && touched.root_url && errors.root_url}
                                    />
                                )}
                            </>
                            }
                            <GeneralButton
                                type="submit"
                                className="modal-form__btn"
                                variant="outlined">
                                {initValues ? "Сохранить" : "Создать"}
                            </GeneralButton>
                        </form>
                    )
                }
            </Formik>
        </AdminModal>
    );
};

const mapDispatchToProps = dispatch => ({
    fetchCreateAdvertType: data => dispatch(fetchCreateAdvertType(data)),
    fetchEditAdvertType: (data, id) => dispatch(fetchEditAdvertType(data, id)),

    fetchCreateSocialNetwork: data => dispatch(fetchCreateSocialNetwork(data)),
    fetchEditSocialNetwork: (data, id) => dispatch(fetchEditSocialNetwork(data, id)),

    fetchCreateContentType: data => dispatch(fetchCreateContentType(data)),
    fetchContentType: (data, id) => dispatch(fetchContentType(data, id)),

    fetchCreateStatus: data => dispatch(fetchCreateStatus(data)),
    fetchEditStatus: (data, id) => dispatch(fetchEditStatus(data, id)),

    fetchCreateAudienceAge: data => dispatch(fetchCreateAudienceAge(data)),
    fetchEditAudienceAge: (data, id) => dispatch(fetchEditAudienceAge(data, id)),

})

export default connect(null, mapDispatchToProps)(SettingsModal);