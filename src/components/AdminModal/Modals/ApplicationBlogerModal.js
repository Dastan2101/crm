import React, {useEffect, useState} from 'react';
import AdminModal from "../AdminModal";
import {Formik} from "formik";
import InputForm from "../../kit/InputForm/InputForm";
import InputPhoneMask from "../../kit/InputPhoneMask/InputPhoneMask";
import GeneralButton from "../../kit/GeneralButton/GeneralButton";

import './index.scss';
import {
    fetchCreateApplicationBlogger,
    fetchEditApplicationBlogger,
    fetchSocialNetworks
} from "../../../store/applications/applicationsActions";
import {connect} from "react-redux";
import {isEmptyArray} from "../../../global/helpers";
import CustomSelect from "../../kit/SocialSelect/CustomSelect";

const MODAL_TYPES = {
    create: "Создать заявку",
    edit: "Редактирование"
}

const ApplicationBlogerModal = (
    {
        onClose,
        type = "create",
        fetchCreateApplicationBlogger,
        initValues,
        fetchEditApplicationBlogger,
        fetchSocialNetworks,
        socialNetworks
    }) => {

    const [state, setState] = useState({
        name: initValues ? initValues.name : "",
        surname: initValues ? initValues.surname : "",
        middle_name: initValues ? initValues.middle_name : "",
        phone_number: initValues ? initValues.phone_number : "",
        email: initValues ? initValues.email : "",
    })

    const [inputFields, setInputFields] = useState([
        {social_network: '', link: ''}
    ]);

    useEffect(function () {
        fetchSocialNetworks()
        if (initValues) {
            setInputFields(initValues.social_network)
        }
    }, [fetchSocialNetworks])



    const handleInputChange = (index, event, item) => {
        const values = [...inputFields];
        if (event.target.name === "link") {
            values[index].link = event.target.value;
        } else {
            values[index].social_network = item;
        }

        setInputFields(values);
    };

    const handleAddFields = () => {
        const values = [...inputFields];
        values.push({social_network: '', link: ''});
        setInputFields(values);
    };

    function onSubmit(values) {
        const data = {...values};
        data.social_network = [...inputFields]

        if (type === "create") {
            fetchCreateApplicationBlogger(data)
        }
        if (type === "edit") {
            fetchEditApplicationBlogger(initValues.id, data)
        }
        onClose()
    }

    function validateForm(values) {
        const errors = {};
        if (!values.name) {
            errors.name = 'Обязательное поле';
        }
        if (!values.middle_name) {
            errors.middle_name = 'Обязательное поле';
        }
        if (!values.surname) {
            errors.surname = 'Обязательное поле';
        }
        if (!values.phone_number) {
            errors.phone_number = 'Обязательное поле';
        }
        if (!values.email) {
            errors.email = 'Обязательное поле';
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
            errors.email = 'Некорректный адрес';
        }
        return errors;
    }

    return (
        <AdminModal onClose={onClose} title={MODAL_TYPES[type]} application={initValues} backdrop={false}>
            <Formik
                initialValues={state}
                validate={values => validateForm(values)}
                onSubmit={(values) => {
                    onSubmit(values)
                }}
            >
                {
                    ({
                         values,
                         errors,
                         touched,
                         handleChange,
                         dirty,
                         handleSubmit,
                     }) => (
                        <form className="modal-form" onSubmit={handleSubmit}>
                            <InputForm
                                label="Фамилия"
                                placeholder="Введите фамилия"
                                propertyName="surname"
                                value={values.surname}
                                onChange={handleChange}
                                isError={errors.surname && touched.surname && errors.surname}
                                helperText={errors.surname && touched.surname && errors.surname}
                            />
                            <InputForm
                                label="Имя"
                                propertyName="name"
                                value={values.name}
                                onChange={handleChange}
                                placeholder="Введите имя"
                                isError={errors.name && touched.name && errors.name}
                                helperText={errors.name && touched.name && errors.name}
                            />
                            <InputForm
                                label="Отчество"
                                propertyName="middle_name"
                                value={values.middle_name}
                                onChange={handleChange}
                                placeholder="Введите отчество"
                                isError={errors.middle_name && touched.middle_name && errors.middle_name}
                                helperText={errors.middle_name && touched.middle_name && errors.middle_name}
                            />
                            {socialNetworks &&
                            !isEmptyArray(socialNetworks) &&
                            <div className="modal-form__socials">
                                <p>соц.сеть *</p>
                                {inputFields.map((field, ndx) => (
                                    <div className="modal-form__socials-fields" key={ndx}>
                                        <CustomSelect
                                            data={socialNetworks}
                                            index={ndx}
                                            onChange={handleInputChange}
                                        />
                                        <InputForm
                                            propertyName="link"
                                            value={field.link}
                                            onChange={event => handleInputChange(ndx, event)}
                                            placeholder="Ссылка на соц.сеть"
                                        />
                                    </div>
                                ))}
                                {socialNetworks && socialNetworks.length > inputFields.length &&
                                <span onClick={handleAddFields} className="modal-form__socials-add">
                                    Добавить еще
                                </span>}
                            </div>
                            }
                            <InputPhoneMask
                                label="Телефон"
                                propertyName="phone_number"
                                value={values.phone_number}
                                onChange={handleChange}
                                isError={errors.phone_number && touched.phone_number && errors.phone_number}
                                helperText={errors.phone_number && touched.phone_number && errors.phone_number}/>
                            <InputForm
                                label="электронная почта"
                                placeholder="Введите почту"
                                value={values.email}
                                propertyName="email"
                                onChange={handleChange}
                                isError={errors.email && touched.email && errors.email}
                                helperText={errors.email && touched.email && errors.email}
                            />
                            <GeneralButton
                                type="submit"
                                className="modal-form__btn"
                                variant="outlined">
                                Сохранить
                            </GeneralButton>
                        </form>
                    )
                }
            </Formik>
        </AdminModal>
    );
};

const mapStateToProps = state => ({
    socialNetworks: state.applicationsInfo.socialNetworks
})

const mapDispatchToProps = dispatch => ({
    fetchCreateApplicationBlogger: data => dispatch(fetchCreateApplicationBlogger(data)),
    fetchEditApplicationBlogger: (id, data) => dispatch(fetchEditApplicationBlogger(id, data)),
    fetchSocialNetworks: () => dispatch(fetchSocialNetworks())
})

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationBlogerModal);