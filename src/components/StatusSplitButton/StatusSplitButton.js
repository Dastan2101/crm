import React, {useRef, useState} from 'react';
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";
import ArrowSimpleIcon from "../kit/Icons/ArrowSimpleIcon";
import Popper from "@material-ui/core/Popper";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import MenuList from "@material-ui/core/MenuList";
import MenuItem from "@material-ui/core/MenuItem";
import './index.scss';

const options = [
    {name: 'Не обработан', color: "#FA445C", code: "N"},
    {name: 'Обработан', color: "#24AF47", code: "D"},
    // {name: 'В процессе', color: "#FBBD0E", code: "P"}
];
const StatusSplitButton = ({initStatus, onChangeStatus, applicationId, disabled}) => {

    let statusIndex = 0;
    const findIndex = options.findIndex(el => el.code === initStatus);
    if (findIndex !== -1) {
        statusIndex = findIndex
    }

    const [open, setOpen] = useState(false);
    const anchorRef = useRef(null);

    const handleMenuItemClick = (event, index) => {
        event.stopPropagation()
        onChangeStatus(applicationId, options[index].code)
        setOpen(false);
    };

    const handleToggle = (e) => {
        e.stopPropagation()
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        event.stopPropagation()
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }
        setOpen(false);
    };
    return (
        <div className="split-button">
            <ButtonGroup variant="contained" color="primary" ref={anchorRef} aria-label="split button">
                <Button>
                    <span style={{color: options[statusIndex].color}}>
                        {options[statusIndex].name}
                    </span>
                </Button>
                {!disabled &&
                <Button
                    color="primary"
                    size="small"
                    aria-controls={open ? 'split-button-menu' : undefined}
                    aria-expanded={open ? 'true' : undefined}
                    aria-label="select merge strategy"
                    aria-haspopup="menu"
                    onClick={handleToggle}
                >
                    <ArrowSimpleIcon/>
                </Button>}
            </ButtonGroup>
            <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                {({TransitionProps, placement}) => (
                    <Grow
                        {...TransitionProps}
                        style={{
                            transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
                        }}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={handleClose}>
                                <MenuList id="split-button-menu" onClick={handleClose}>
                                    {options.map((option, index) => (
                                        <MenuItem
                                            key={index}
                                            selected={index === statusIndex}
                                            disabled={initStatus === option.code}
                                            onClick={(event) => handleMenuItemClick(event, index)}
                                        >
                                            <span style={{color: option.color}}>
                                                {option.name}
                                            </span>
                                        </MenuItem>
                                    ))}
                                </MenuList>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </div>
    );
};

export default StatusSplitButton;