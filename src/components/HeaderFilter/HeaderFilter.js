import React from 'react';
import DatePickerForm from "../kit/DatePickerForm/DatePickerForm";
import SearchIcon from "../kit/Icons/SearchIcon";
import CloseIcon from "../kit/Icons/CloseIcon";
import './index.scss';
import SortSelect from "../kit/SortSelect/SortSelect";

const HeaderFilter = (
    {
        selectedDate,
        dateHandler,
        search,
        inputHandler,
        onClear,
        options,
        sortHandler,
        sortActiveValue
    }) => {
    return (
        <div className="header-filter">
            <div className="header-filter__sort">
                {options && (
                <SortSelect options={options} onChange={sortHandler} sortActiveValue={sortActiveValue} />)}
            </div>
            <div className="header-filter__date">
                <DatePickerForm
                    placeholder="Выбрать дату"
                    selected={selectedDate}
                    onChange={dateHandler}
                    className="header-filter__date-custom"
                />
            </div>
            <div className="header-filter__search">
                <SearchIcon/>
                <input
                    type="search"
                    placeholder="Поиск..."
                    value={search}
                    onChange={inputHandler}/>
            </div>
            {selectedDate || search ?
            <div className="header-filter__clear" onClick={onClear}>
                <p>Очистить</p>
                <CloseIcon width={16} height={16} color="#ccc"/>
            </div> : null
            }
        </div>
    );
};

export default HeaderFilter;