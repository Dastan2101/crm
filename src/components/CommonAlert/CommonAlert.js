import React from 'react';
import AlertTitle from "@material-ui/lab/AlertTitle";
import Alert from "@material-ui/lab/Alert";

import './index.scss';

const CommonAlert = ({type = "warning", status, message}) => {
    return (
        <div className="common-alert">
            <Alert severity={type}>
                {status &&
                <AlertTitle>{status}</AlertTitle>}
                {message} — <strong>попробуйте позднее!</strong>
            </Alert>
        </div>
    );
};

export default CommonAlert;