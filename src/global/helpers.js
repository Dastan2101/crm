import {useEffect, useState} from "react";
import moment from 'moment';

export const numberWithSpaces = (number = 0) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
};

export const dateFormatter = dateTime => {
    const date = new Date(dateTime);
    return `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`
};

export const isEmptyArray = array => {
    return array.length === 0;
}

export const getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
}

export const cutText = (text, length) => {
    return text ? `${text.slice(0, length)} ${length > text.length ? "" : "..."}` : ""
}

export const convertDateWithMonthName = datetime => {
    const convertDate = moment(datetime,"DD-MM-YYYY")
    const date = new Date(convertDate);
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    const monthTranslate = [
        'января',
        'февраля',
        'марта',
        'апреля',
        'мая',
        'июня',
        'июля',
        'августа',
        'сентября',
        'октября',
        'ноября',
        'декабря'][month];

    return `${day} ${monthTranslate} ${year}`
};

export const numberWithDot = (sum) => {
    return sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

export const getPercentage = (number, percent) => {
    return !isNaN(Number(number)) ? Math.floor(number * percent / 100) : 0;
}

export function useDebounce(value, delay) {
    const [debouncedValue, setDebouncedValue] = useState(value);

    useEffect(
        () => {
            const handler = setTimeout(() => {
                setDebouncedValue(value);
            }, delay);
            return () => {
                clearTimeout(handler);
            };
        },
        [value, delay]
    );

    return debouncedValue;
}
